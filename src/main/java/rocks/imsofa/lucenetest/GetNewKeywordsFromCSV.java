/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Main.java to edit this template
 */
package rocks.imsofa.lucenetest;

import com.hankcs.hanlp.HanLP;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVRecord;
import org.apache.commons.io.FileUtils;

/**
 *
 * @author lendle
 */
public class GetNewKeywordsFromCSV {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) throws Exception {
        // TODO code application logic here
        File csvFile = new File("4M.csv");
        HanLPSetupUtil.setupCustomDictAndStopWords();
        Set<String> set = new HashSet<>();
        List<String> lines = FileUtils.readLines(new File("dict.txt"), "utf-8");
        for (String line : lines) {
            if (line.length() > 30) {
//                System.out.println(line);
            } else if (line.matches("[\\d\\.]+")) {
//                System.out.println(line);
            } else {
                set.add(line);
            }
        }
        Map<String, String> newSet = new HashMap<>();
        try (Reader in = new InputStreamReader(new FileInputStream(csvFile), "utf-8")) {
            Iterable<CSVRecord> records = CSVFormat.Builder.create(CSVFormat.EXCEL).setHeader().build().parse(in);
            for (CSVRecord record : records) {
                if (record.get("需求者意見描述") == null) {
                    continue;
                }
                List<String> keywords = HanLP.extractKeyword(record.get("需求者意見描述"), 3);
                for (String key : keywords) {
                    if (key.length() > 1 && set.contains(key) == false && !key.matches("[\\d\\.]+")) {
//                        System.out.println(key);
                        newSet.put(key, "");
                    }
                }

            }
        }
        for (String key : newSet.keySet()) {
            System.out.println(key);
        }
    }

}
