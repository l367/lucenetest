/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Main.java to edit this template
 */
package rocks.imsofa.lucenetest;

import com.github.kevinsawicki.http.HttpRequest;
import com.google.gson.Gson;
import java.io.File;
import java.io.IOException;
import java.net.URLDecoder;
import java.net.URLEncoder;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.StringEscapeUtils;
import org.apache.poi.xwpf.usermodel.BreakClear;

/**
 *
 * @author lendle
 */
public class EnhanceKeywordsUsingGoogle {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) throws Exception {
        // TODO code application logic here
        File dict = new File("dict1.txt");
        Map<String, String> keyMap = new HashMap<>();
        int count = 0;
        int start = 1;
        int index = 0;

        List<String> lines = FileUtils.readLines(dict, "utf-8");
        for (String line : lines) {
            line=URLEncoder.encode(line, "utf-8");
            count++;
            try {
                System.out.println(count+":"+line);
                Thread.sleep(5000);

                String key = line;
                String src = HttpRequest.get("https://customsearch.googleapis.com/customsearch/v1?cx=967d66347cb334550&key=AIzaSyCG_vwPBCAp1DZZogsuP0-yCJWBAQDxZVY&lr=lang_zh-TW&start=" + start + "&q=" + key).body();
//        System.out.println(src);
                Gson gson = new Gson();
                Map topMap = gson.fromJson(src, Map.class);
                List<Map> items = (List) topMap.get("items");

                if (items != null) {
                    for (Map item : items) {
                        String htmlSnippet = (String) item.get("htmlSnippet");
                        if (htmlSnippet == null) {
                            continue;
                        }
//                        System.out.println(htmlSnippet);
                        htmlSnippet = StringEscapeUtils.unescapeJava(htmlSnippet);
//                        System.out.println(htmlSnippet);
                        Pattern p = Pattern.compile("<b>[^<]+</b>");
                        Matcher m = p.matcher(htmlSnippet);
                        while (m.find()) {
                            String group = m.group();
                            int index1 = group.indexOf(">");
                            int index2 = group.indexOf("<", index1 + 1);
                            group = group.substring(index1 + 1, index2);
                            if (group.equals("...") || group.length()<2 || group.length()>10) {
                                continue;
                            }
                            System.out.println("key="+URLDecoder.decode(key, "utf-8")+", new key="+group);
                            keyMap.put(group, "");
                        }
                        if (item.containsKey("pagemap")) {
                            Map map = (Map) item.get("pagemap");
                            if (map.containsKey("website")) {
                                List<Map> webSites = (List) map.get("website");
                                for (Map map1 : webSites) {
                                    if (map1.containsKey("keywords")) {
                                        String keywords = (String) map1.get("keywords");
                                        String[] keywordsArray = keywords.split("(\\s)*,(\\s)*");
                                        for (String k : keywordsArray) {
                                            keyMap.put(k, "");
                                        }
                                    }
                                }
                            }
                        }

                        index++;
                    }
                }
            } catch (Exception e){
                e.printStackTrace();
            }
        }

        if (keyMap.size() > 0) {
            System.out.println(keyMap.keySet());
        }
        String newDictString = String.join("\r\n", keyMap.keySet());
        FileUtils.write(new File("newdict.txt"), newDictString, "utf-8");
    }

}
