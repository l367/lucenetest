/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Main.java to edit this template
 */
package rocks.imsofa.lucenetest;

import com.hankcs.lucene.HanLPAnalyzer;
import java.nio.file.Files;
import java.nio.file.Path;
import org.apache.lucene.document.Document;
import org.apache.lucene.document.Field;
import org.apache.lucene.document.TextField;
import org.apache.lucene.index.IndexWriter;
import org.apache.lucene.index.IndexWriterConfig;
import org.apache.lucene.store.Directory;
import org.apache.lucene.store.FSDirectory;

/**
 *
 * @author USER
 */
public class CreateIndex1 {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) throws Exception {
        // TODO code application logic here
        HanLPAnalyzer analyzer = new HanLPAnalyzer2();
        Path indexPath = Files.createDirectories(Path.of("index"));
        Directory directory = FSDirectory.open(indexPath);
        IndexWriterConfig config = new IndexWriterConfig(analyzer);
        config.setOpenMode(IndexWriterConfig.OpenMode.APPEND);
        IndexWriter iwriter = new IndexWriter(directory, config);

        Document doc = new Document();
        doc.add(new TextField("title", "喉嚨異物感", Field.Store.YES));
        doc.add(new TextField("text", "感受到喉嚨有異物感，但用力咳只咳出一些痰，於是拿手電筒往嘴巴裡照居然見到一點一點白色顆粒，立刻拿出棉花棒把白頭旁邊的肉稍微翻開，結果發現扁桃腺裡藏著整排的結石", Field.Store.YES));
        iwriter.addDocument(doc);
        iwriter.close();
    }

}
