/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Main.java to edit this template
 */
package rocks.imsofa.lucenetest;

import com.hankcs.lucene.HanLPAnalyzer;
import java.nio.file.Files;
import java.nio.file.Path;
import org.apache.lucene.analysis.Analyzer;
import org.apache.lucene.document.Document;
import org.apache.lucene.index.DirectoryReader;
import org.apache.lucene.index.Term;
import org.apache.lucene.queryparser.classic.MultiFieldQueryParser;
import org.apache.lucene.queryparser.classic.QueryParser;
import org.apache.lucene.search.IndexSearcher;
import org.apache.lucene.search.MultiPhraseQuery;
import org.apache.lucene.search.Query;
import org.apache.lucene.search.TopDocs;
import org.apache.lucene.store.Directory;
import org.apache.lucene.store.FSDirectory;

/**
 *
 * @author USER
 */
public class DoQuery {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) throws Exception {
        // TODO code application logic here
        Path indexPath = Files.createDirectories(Path.of("index"));
        Directory directory = FSDirectory.open(indexPath);
        DirectoryReader ireader = DirectoryReader.open(directory);
        IndexSearcher isearcher = new IndexSearcher(ireader);

        HanLPAnalyzer2 analyzer = new HanLPAnalyzer2();
        HanLPSetupUtil.setupCustomDictAndStopWords();
        MultiFieldQueryParser multiFieldQueryParser = new MultiFieldQueryParser(new String[]{"title", "text", "date"}, analyzer);
        Query query = multiFieldQueryParser.parse("date:[20230701 TO 20230731] AND  (tile:(+\"貸款\" +\"紓困\") OR text:(+\"貸款\" +\"紓困\"))");
//        FuzzyQuery query=new FuzzyQuery(new Term("text", "超人"));
        TopDocs topDocs = isearcher.search(query, 10);
        for (int i = 0; i < topDocs.scoreDocs.length; i++) {
            Document hitDoc = isearcher.doc(topDocs.scoreDocs[i].doc);
            System.out.println(hitDoc.get("title")+":"+hitDoc.get("link"));
        }
        ireader.close();
        directory.close();
    }

}
