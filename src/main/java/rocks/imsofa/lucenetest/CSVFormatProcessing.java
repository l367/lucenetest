/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package rocks.imsofa.lucenetest;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Iterator;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellType;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

/**
 *
 * @author s1088
 */
public class CSVFormatProcessing {

    public static File tocsvformat(File inputFile) {
        if(inputFile.getName().toLowerCase().endsWith(".csv")){
            return inputFile;
        }
        FileInputStream fis = null;
        String filename = inputFile.getName().substring(0, inputFile.getName().indexOf("."));
        File outputFile = new File(filename + ".csv");
        try {
            // For storing data into CSV files
            StringBuffer data = new StringBuffer();
            fis = new FileInputStream(inputFile);
            Workbook workbook = null;
            if (inputFile.getName().endsWith(".xlsx")) {
                workbook = new XSSFWorkbook(fis);
            } else if (inputFile.getName().endsWith(".xls")) {
                workbook = new HSSFWorkbook(fis);
            } else {
                fis.close();
                throw new Exception("File not supported!");
            }
            // Get first sheet from the workbook
            Sheet sheet = workbook.getSheetAt(0);
            // Iterate through each rows from first sheet
            Iterator<Row> rowIterator = sheet.iterator();
 
            while (rowIterator.hasNext()) {
                Row row = rowIterator.next();
                boolean first=true;
                for(int i=0; i<18; i++){
                    if(!first){
                        data.append(",");
                    }
                    first=false;
                    Cell cell=row.getCell(i, Row.MissingCellPolicy.RETURN_BLANK_AS_NULL);
                    String text=""+cell;
                    text=text.replace('\"', ' ');
                    data.append("\"").append(text).append("\"");
                }
                data.append("\r\n");
                
            }
 
            FileOutputStream fos = new FileOutputStream(outputFile);
            fos.write(data.toString().getBytes());
            fos.close();
        } catch (FileNotFoundException ex) {
            Logger.getLogger(CSVFormatProcessing.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(CSVFormatProcessing.class.getName()).log(Level.SEVERE, null, ex);
        } catch (Exception ex) {
            Logger.getLogger(CSVFormatProcessing.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            try {
                fis.close();
            } catch (IOException ex) {
                Logger.getLogger(CSVFormatProcessing.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        System.out.println(""+outputFile.getName());
        return outputFile;
    }
}


