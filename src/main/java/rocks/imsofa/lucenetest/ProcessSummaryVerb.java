/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Main.java to edit this template
 */
package rocks.imsofa.lucenetest;

import com.google.gson.Gson;
import com.hankcs.hanlp.HanLP;
import com.hankcs.hanlp.seg.common.Term;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.io.Reader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVRecord;
import org.apache.commons.io.FileUtils;

/**
 *
 * @author lendle
 */
public class ProcessSummaryVerb {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) throws Exception {
        // TODO code application logic here
        //setup hanlp custom dictionary
//        HanLPSetupUtil.setupCustomDictAndStopWords();
        /////////////////////////////////////
//        File sourceFile=new File("3M-ALL_TEXT (元智團隊).xlsx");
        Gson gson=new Gson();
        String json=FileUtils.readFileToString(new File("db.json"), "utf-8");
        List<Map<String, Object>> db=gson.fromJson(json, List.class);
        
//        File sourceFile = new File("1-7m(去識別).csv");
        Set<String> verbsSet=new HashSet<>();
        Set<String> adSet=new HashSet<>();
        List<String> contents=new ArrayList<>();
        for(Map<String, Object> map : db){
            double emotion=Double.valueOf(""+map.get("requestorCommentEmotion"));
            if(emotion<0){
                contents.add(""+map.get("summary"));
            }
        }
        
        for(String content : contents){
            List<Term> terms = HanLP.segment(content);
                for (Term term : terms) {
                    if (term.word.length()>1 && term.nature.toString().startsWith("v")) {
                        verbsSet.add(term.word);
//                        System.out.println(term.word + ":" + term.nature);
                        if(term.word.equals("抱怨")){
                            System.out.println(content);
                            System.out.println("\t抱怨");
//                            break;
                        }
                    }else if(term.word.equals("滿意")){
                        System.out.println(content);
                        System.out.println("\t滿意");
//                        break;
                    }else if (term.word.length()>1 && term.nature.toString().startsWith("a")) {
                        adSet.add(term.word);
                    }
                }
        }
       
        System.out.println(verbsSet);
        System.out.println(adSet);
        
        List<String> verbs=new ArrayList<>(verbsSet);
        List<String> ads=new ArrayList<>(adSet);
        List<String> all=new ArrayList<>(verbs);
//        all.addAll(ads);
        
        Map<String, List<Integer>> wordMatrix=new HashMap<>();
        for(String str : all){
            List<Integer> row=new ArrayList<>();
            for(int i=0; i<all.size(); i++){
                row.add(0);
            }
            wordMatrix.put(str, row);
        }
        for(String content : contents){
            for(String s1 : all){
                if(content.contains(s1)){
                    List<Integer> row=wordMatrix.get(s1);
                    for(int i=0; i<row.size(); i++){
                        String s2=all.get(i);
                        if(s1.equals(s2)){
                            row.set(i, 0);
                        }else{
                            if(content.contains(s2)){
                                row.set(i, row.get(i)+1);
                            }
                        }
                    }
                }
            }
        }
//        for(String v : verbsSet){
//            List<Integer> row=new ArrayList<>();
//            wordMatrix.put(v, row);
//            for(String content : contents){
//                if(content.contains(v)){
//                    row.add(1);
//                }else{
//                    row.add(0);
//                }
//            }
//        }
//        for(String v : adSet){
//            List<Integer> row=new ArrayList<>();
//            wordMatrix.put(v, row);
//            for(String content : contents){
//                if(content.contains(v)){
//                    row.add(1);
//                }else{
//                    row.add(0);
//                }
//            }
//        }
        System.out.println(wordMatrix);
        try(PrintWriter writer=new PrintWriter(new OutputStreamWriter(new FileOutputStream("va_matrix.csv"), "utf-8"))){
            writer.print("word");
            int index=0;
            for(String str : all){
                writer.print(","+str);
            }
            writer.println();
            for(String str : all){
                List<Integer> row=wordMatrix.get(str);
                writer.print(str);
                for(int value : row){
                    writer.print(","+(value));
                }
                writer.println();
            }
//            for(String word : wordMatrix.keySet()){
//                writer.print(word);
//                List<Integer> row=wordMatrix.get(word);
//                for(int value : row){
//                    writer.print(","+(value));
//                }
//                writer.println();
//            }
        }
    }

}
