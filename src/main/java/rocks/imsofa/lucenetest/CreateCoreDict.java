/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Main.java to edit this template
 */
package rocks.imsofa.lucenetest;

import com.hankcs.hanlp.HanLP;
import com.hankcs.hanlp.corpus.tag.Nature;
import com.hankcs.hanlp.dictionary.CustomDictionary;
import com.hankcs.hanlp.dictionary.stopword.CoreStopWordDictionary;
import com.hankcs.hanlp.restful.HanLPClient;
import com.hankcs.hanlp.seg.common.Term;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.Reader;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVRecord;
import org.apache.commons.io.FileUtils;

/**
 *
 * @author USER
 */
public class CreateCoreDict {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) throws Exception {
        // TODO code application logic here
        //setup hanlp custom dictionary
        HanLPSetupUtil.setupCustomDictAndStopWords();

        File dict = new File("base_dict.txt");
        File newDict = new File("dict.txt");
        FileUtils.copyFile(dict, newDict);
        File dir = new File("indexing");
        Map<String, String> allTokens = new HashMap<>();
        StringBuilder buffer = new StringBuilder();

        for (File file : dir.listFiles()) {
            List<String> lines = FileUtils.readLines(file, "utf-8");
            String title = lines.remove(0);
            String link = lines.remove(0);
            String text = String.join("\r\n", lines.toArray(new String[0]));
            if (text.length() > 5000) {
                text = text.substring(0, 5000);
            }
            List<Term> terms = HanLP.segment(title);
            for (Term term : terms) {
                if (term.word.length() >= 3 && allTokens.containsKey(term.word) == false) {
                    allTokens.put(term.word, "");
                    System.out.println(term.word.trim());
                    buffer.append("\r\n").append(term.word.trim());
                }
//                if(term.nature.equals(Nature.nr) || term.nature.equals(Nature.ns) || term.nature.equals(Nature.nt) || term.nature.equals(Nature.ntc)
//                        || term.nature.equals(Nature.ntcb) || term.nature.equals(Nature.nto) || term.nature.equals(Nature.nts) || term.nature.equals(Nature.ntu)){
//                    if(term.word.length()>=3 && allTokens.containsKey(term.word)==false){
//                        allTokens.put(term.word, "");
//                        System.out.println(term.word);
//                        buffer.append("\r\n").append(term.word);
//                    }
//                }
            }
            FileUtils.write(new File("dict.txt"), buffer.toString(), "utf-8", true);
            buffer = new StringBuilder();
        }
        if (buffer.length() > 0) {
            FileUtils.write(new File("dict.txt"), buffer.toString(), "utf-8", true);
        }

    }

}
