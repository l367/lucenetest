/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Main.java to edit this template
 */
package rocks.imsofa.lucenetest;

import com.hankcs.hanlp.HanLP;
import com.hankcs.hanlp.restful.HanLPClient;
import java.io.FileReader;
import java.io.Reader;
import java.io.StringReader;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVRecord;
import org.apache.commons.io.IOUtils;

/**
 *
 * @author lendle
 */
public class TestFullTextQueryRecord {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) throws Exception {
        //setup hanlp custom dictionary
        HanLPSetupUtil.setupCustomDictAndStopWords();
        /////////////////////////////////////
//        File dataFile = CSVFormatProcessing.tocsvformat(new File("112_1_2.xlsx"));
        Reader in = new FileReader("112_1_2.csv");
        Iterable<CSVRecord> records = CSVFormat.Builder.create(CSVFormat.EXCEL).setHeader().build().parse(in);
        List<String> recordlList = new ArrayList<>();
        HanLPClient client = new HanLPClient("https://www.hanlp.com/api", "MjE5MkBiYnMuaGFubHAuY29tOjhlM1U5dFB2WnNGaHI2VGw=");
        for (CSVRecord record : records) {
//            System.out.println(record.size());
            HashMap<String, List<String>> optionhMap = new HashMap<>();
            List<String> SPK1_lines = new ArrayList<>();
            List<String> SPK2_lines = new ArrayList<>();
            optionhMap.put("SPK:1", SPK1_lines);
            optionhMap.put("SPK:2", SPK2_lines);
            String removeinfo_id = null;
            String option = record.get("音檔敘述說明");

            if (option.trim().length() == 0) {
                continue;
            }

            List<String> lines = IOUtils.readLines(new StringReader(option));

            for (int i = 0; i < lines.size(); i++) {
                if (lines.get(i).equals("SPK:1")) {
                    optionhMap.get("SPK:1").add(lines.get(i + 2));
                } else if (lines.get(i).equals("SPK:2")) {
                    optionhMap.get("SPK:2").add(lines.get(i + 2));
                }
                if (lines.get(i).contains("貴姓") || lines.get(i).contains("稱呼")) {
                    removeinfo_id = lines.get(i - 2);
                }
            }
            HashMap<String, List<String>> origOptionhMap = new HashMap<>(optionhMap);
            if (optionhMap.size() > 1) {
                optionhMap.remove(removeinfo_id);
            }
            if (optionhMap.containsKey("SPK:1") && optionhMap.get("SPK:1").isEmpty()) {
                optionhMap.remove("SPK:1");
            }
            if (optionhMap.containsKey("SPK:2") && optionhMap.get("SPK:2").isEmpty()) {
                optionhMap.remove("SPK:2");
            }
            String remainingText = (optionhMap.isEmpty()) ? "[]" : optionhMap.get(optionhMap.keySet().toArray(new String[0])[0].toString()).toString();
            if (remainingText.length() < 10) {
                List<String> all = new ArrayList<>();
                for (List<String> value : origOptionhMap.values()) {
                    all.addAll(value);
                    all.add(" ");
                }
                remainingText = all.toString();
            }
            recordlList.add(remainingText);
        }
        for (String str : recordlList) {
            str=HanLP.getSummary(str, 500);
//            str = (str.length() > 1000) ? str.substring(0, 1000) : str;
            while (true) {
                try {
                    double value = client.sentimentAnalysis(str);
                    System.out.println(value + ":" + str);
                    break;
                } catch (Throwable e) {
                    System.out.println("timeout, restart......");
                    e.printStackTrace();
                    Thread.sleep(10000);
                }
            }

            Thread.sleep(2000);
        }

    }

}
