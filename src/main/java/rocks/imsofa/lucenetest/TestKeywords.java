/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 */
package rocks.imsofa.lucenetest;

import java.io.File;
import java.io.Reader;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import org.apache.commons.csv.*;
import org.apache.commons.io.FileUtils;
import com.hankcs.hanlp.*;
import com.hankcs.hanlp.mining.word.WordInfo;
import com.hankcs.hanlp.seg.Segment;
import com.hankcs.hanlp.seg.common.Term;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.HashMap;

/**
 *
 * @author lendle_win10
 */
public class TestKeywords {

    public static void main(String[] args) throws Exception {
        File logFile = new File("test.csv");
        File csvFile = CSVFormatProcessing.tocsvformat(logFile);
        HanLPSetupUtil.setupCustomDictAndStopWords();
        Map<String, String> keywordsMap = new HashMap<>();
        Map<String, String> stopWords = new HashMap<>();
        File dict = new File("dict.txt");
        try {
            List<String> lines = FileUtils.readLines(dict, "utf-8");
            for (String line : lines) {
                if (line.trim().length() > 1) {
                    keywordsMap.put(line, "");
                }
            }
        } catch (IOException ex) {
            //Logger.getLogger(HanLPSetupUtil.class.getName()).log(Level.SEVERE, null, ex);
        }
        Segment segment = HanLP.newSegment().enableAllNamedEntityRecognize(true).enableCustomDictionary(true).enableCustomDictionaryForcing(true).enableIndexMode(2);
        try (Reader in = new InputStreamReader(new FileInputStream(csvFile), "big5")) {
            Iterable<CSVRecord> records = CSVFormat.Builder.create(CSVFormat.EXCEL).setHeader().build().parse(in);
            for (CSVRecord record : records) {
                /*if (record.get("音檔敘述說明") == null) {
                    continue;
                }*/

                //System.out.println(record.get("需求者意見選項"));
                //System.out.println(record.get("需求者意見描述"));
                String str = record.get("需求者意見描述");
//                str=str.replace("資格"," 資格 ");
//                List<String> keywords = HanLP.extractKeyword(str, 10);
//                List<String> keywords = HanLP.extractPhrase(str, 10);
                List<Term> words = segment.seg(str);
//                System.out.println(words.size());
                List<String> keywords = new ArrayList<>();
                for (Term wordInfo : words) {
                    if (wordInfo.word.length() >= 2 && wordInfo.word.contains(",") == false) {
//                        System.out.println(wordInfo.getFrequency()+":"+wordInfo.word);
                        keywords.add(wordInfo.word);
                    }
                }
                keywords = HanLP.extractKeyword(String.join(",", keywords), 10);
                System.out.println(keywords + ":" + str);
            }

        }
        /*for(String s : stopWords.keySet()){
            System.out.println(s);
        }*/
        String testNews = FileUtils.readFileToString(new File("test.txt"), "utf-8");
        List<Term> words = segment.seg(testNews);
        List<String> keywords = new ArrayList<>();
        for (Term wordInfo : words) {
            if (wordInfo.word.length() >= 2 && wordInfo.word.contains(",") == false) {
                        System.out.println(wordInfo.getFrequency()+":"+wordInfo.word);
                keywords.add(wordInfo.word);
            }
        }
        keywords = HanLP.extractKeyword(String.join(",", keywords), 10);
        System.out.println(keywords);
    }
}
