/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Main.java to edit this template
 */
package rocks.imsofa.lucenetest;

import org.renjin.script.RenjinScriptEngine;

/**
 *
 * @author USER
 */
public class TestIndicatorAnalysis {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) throws Exception {
        // TODO code application logic here
        RenjinScriptEngine engine = new RenjinScriptEngine();
        engine.eval("library('randomForest')");
        engine.eval("rf=readRDS('indicator_analysis.RDS')");
        Object o=engine.eval("predict(rf, data.frame(v4=c(0), v5=c(0)))");
        System.out.println(o);
    }

}
