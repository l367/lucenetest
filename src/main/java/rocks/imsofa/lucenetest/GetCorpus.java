/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Project/Maven2/JavaApp/src/main/java/${packagePath}/${mainClassName}.java to edit this template
 */
package rocks.imsofa.lucenetest;

import com.github.kevinsawicki.http.HttpRequest;
import com.google.gson.Gson;
import com.hankcs.hanlp.restful.HanLPClient;
import java.io.File;
import java.io.FileReader;
import java.io.Reader;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVRecord;
import org.apache.commons.io.FileUtils;
import org.apache.poi.sl.usermodel.StrokeStyle;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

/**
 *
 * @author USER
 */
public class GetCorpus {

    public static void main(String[] args) throws Exception {
        if (1 != 1) {
            Reader in = new FileReader("112_1_2.csv");
            Set<String> options = new HashSet<>();
            Iterable<CSVRecord> records = CSVFormat.Builder.create(CSVFormat.EXCEL).setHeader().build().parse(in);
            HanLPClient client = new HanLPClient("https://www.hanlp.com/api", "MjE5MkBiYnMuaGFubHAuY29tOjhlM1U5dFB2WnNGaHI2VGw=");
            Set<String> keyOptions = new HashSet<>();
            for (CSVRecord record : records) {
                String option = record.get("需求者意見選項");
                options.add(option);
            }
            for (String option : options) {
                Map<String, Double> keyPhrases = client.keyphraseExtraction(option);
                List<Map.Entry<String, Double>> keyPhrasesList = new ArrayList<Map.Entry<String, Double>>(keyPhrases.entrySet());
                Collections.sort(keyPhrasesList, new Comparator<Map.Entry<String, Double>>() {
                    @Override
                    public int compare(Map.Entry<String, Double> o1, Map.Entry<String, Double> o2) {
                        return (int) (o2.getValue() - o1.getValue());
                    }
                });

                List<String> output = new ArrayList<>();
                int count = 0;
                for (Map.Entry<String, Double> phrase : keyPhrasesList) {
                    if (phrase.getKey().equals(option)) {
                        continue;
                    } else {
                        output.add(phrase.getKey());
                        count++;
                    }
                    if (count == 2) {
                        break;
                    }
                }
                if (output.isEmpty()) {
                    output.add(option);
                }
                String keyword = String.join(" ", output.toArray(new String[0])).replaceAll("類", "");

                keyOptions.add(keyword);

                Thread.sleep(1000);
            }
            System.out.println(keyOptions);
        }

        int count = 0;
        int start = 1;
        int index = 0;
        boolean startGoogle = true;
//        Set<String> keyOptions = new HashSet<>(List.of("資金週轉", "青創貸款", "紓困貸款", "能源政策", "勞工", "中小企業", "SBIR", "債務協商", "會計制度", "票款清償", "經濟部", "能源政策", "財稅法規"));
        Set<String> keyOptions = new HashSet<>(List.of(/*"銀行","貸款","啟動","青創","展延","中小企業","創新","債務","額度",*/"機構","資金週轉", "青創貸款", "紓困貸款", "能源政策", "勞工", "中小企業", "SBIR", "債務協商", "會計制度", "票款清償", "經濟部", "能源政策", "財稅法規"));
        List<String> ymList = List.of(/*"2023-01", "2023-02", "2023-03", "2023-04", "2023-05"*/"2023-06");
        Gson gson = new Gson();
        for (String ym : ymList) {
            String startYM = ym;
            String endYM = ym;
            System.out.println("ym="+ym);
            File folder=new File("indexing", ym.replace("-", ""));
            folder.mkdirs();
            File hashtableFile = new File(folder, ".hashtable.json");
            Map<String, String> map = hashtableFile.exists()?gson.fromJson(FileUtils.readFileToString(hashtableFile, "utf-8"), Map.class):new HashMap<>();
            for (String key : keyOptions) {
                if (!startGoogle) {
                    continue;
                }
                System.out.println("processing key=" + key);
                key = URLEncoder.encode(key, "utf-8");
                count = 0;
                start = 1;
                while (count < 30 && start != -1) {
                    String src = HttpRequest.get("https://customsearch.googleapis.com/customsearch/v1?cx=967d66347cb334550&key=AIzaSyCG_vwPBCAp1DZZogsuP0-yCJWBAQDxZVY&lr=lang_zh-TW&sort=date:r:"+ym.replace("-", "")+"01"+":"+ym.replace("-", "")+"31"+"&start=" + start + "&q=" + key).body();
                    Map topMap = gson.fromJson(src, Map.class);
                    List<Map> items = (List) topMap.get("items");
                    if (items != null) {
                        for (Map item : items) {
                            String date = (String) ((Map) ((List) ((Map) item.get("pagemap")).get("metatags")).get(0)).get("date");
                            if (date == null) {
                                continue;
                            }
                            date = date.substring(0, 10);
                            //String title = (String) item.get("title");

                            String link = (String) item.get("link");
                            if(map.containsKey(link)){
                                continue;
                            }
                            Map<String, String> newsData = extractNewsData(link);
                            String text = newsData.get("text");
                            String title = newsData.get("title");
                            date = newsData.get("date");
                            if (date.length() < 7) {
                                continue;
                            }
                            if (date.substring(0, 7).compareTo(startYM) < 0 || date.substring(0, 7).compareTo(endYM) > 0) {
                                System.out.println(date);
                                continue;
                            }
                            System.out.println(date + ":" + title);
                            StringBuilder sb = new StringBuilder();
                            sb.append(title).append("\r\n").append(date).append("\r\n").append(link).append("\r\n").append(text);
                            File file = new File(folder, "" + System.currentTimeMillis() + ".txt");
                            FileUtils.write(file, sb.toString(), "utf-8");
                            count++;
                            index++;
                        }
                    }
                    Map queries = (Map) topMap.get("queries");
                    if (queries!=null && queries.containsKey("nextPage")) {
                        start = start + 10;
                    } else {
                        break;
                    }
                    Thread.sleep(20000);
                }
            }
        }
    }

    private static Map<String, String> extractNewsData(String link) throws Exception {
        String text = HttpRequest.get(link).body();
        Document doc = Jsoup.parse(text);
        if (link.contains("money.udn")) {
            String title = doc.select(".article-head__title").text().trim();
            String body = doc.select(".article-body__editor").text().trim();
            String date = doc.select(".article-body__time").text().trim();
            date = date.split(" ")[0].replace("/", "-");
            return Map.of("title", title, "date", date, "text", body);
        } else if (link.contains("udn.com")) {
            String title = doc.select(".article-content__title").text().trim();
            String date = doc.select(".article-content__time").text().trim().split(" ")[0];
            String body = doc.select(".article-content").text().trim();
            return Map.of("title", title, "date", date, "text", body);
        } else if (link.contains("chinatimes.")) {
            String title = doc.select(".article-title").text().trim();
            Elements elements = doc.select(".article-body p");
            StringBuffer _sb = new StringBuffer();
            for (int i = 0; i < elements.size(); i++) {
                Element element = elements.get(i);
                _sb.append(element.text()).append("\r\n");
            }
            text = _sb.toString();

            String date = doc.select(".meta-info-wrapper .date").text().trim().replace("/", "-");
            String body = text.trim();
            return Map.of("title", title, "date", date, "text", body);
        } else {
            Elements elements = doc.select("p");
            StringBuffer _sb = new StringBuffer();
            for (int i = 0; i < elements.size(); i++) {
                Element element = elements.get(i);
                _sb.append(element.text()).append("\r\n");
            }
            text = _sb.toString();
            return Map.of("title", "", "date", "", "text", text);
        }
    }
}
