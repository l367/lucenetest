/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Main.java to edit this template
 */
package rocks.imsofa.lucenetest;

import com.google.gson.Gson;
import java.io.File;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.apache.commons.io.FileUtils;

/**
 *
 * @author USER
 */
public class CreateCorpusHashtable {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) throws Exception {
        // TODO code application logic here
        File indexing = new File("indexing");
        Gson gson = new Gson();
        for (File sub : indexing.listFiles()) {
            File hashtableFile = new File(sub, ".hashtable.json");
            Map<String, String> map = new HashMap<>();
            if (hashtableFile.exists()) {
                map = gson.fromJson(FileUtils.readFileToString(hashtableFile, "utf-8"), Map.class);
            }
            for (File file : sub.listFiles()) {
                if (file.getName().equals(".hashtable.json") == false) {
                    List<String> lines = FileUtils.readLines(file, "utf-8");
                    String title = lines.remove(0);
                    String date = lines.remove(0).replace("-", "");
                    String link = lines.remove(0);
                    map.put(link, "");
                }
            }
            FileUtils.write(hashtableFile, gson.toJson(map), "utf-8");
        }
    }

}
