/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 */

package rocks.imsofa.lucenetest;

import com.google.gson.Gson;
import java.io.File;
import java.io.FileReader;
import java.io.Reader;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import org.apache.commons.csv.*;
import org.apache.commons.io.FileUtils;

/**
 *
 * @author lendle_win10
 */
public class GetEmotionScore {

    public static void main(String[] args) throws Exception{
        File logFile=new File("test.csv");
        File csvFile = CSVFormatProcessing.tocsvformat(logFile);
        List<Map<String, Object>> list=new ArrayList<>();
        try (Reader in = new FileReader(csvFile)) {
            Iterable<CSVRecord> records = CSVFormat.Builder.create(CSVFormat.EXCEL).setHeader().build().parse(in);
            for (CSVRecord record : records) {
                if(record.get("音檔敘述說明")==null){
                    continue;
                }
                String monthInData=record.get("服務日期").replace("-", "").substring(0, 6);
               
                System.out.println(record.get("需求者意見選項"));
                System.out.println(record.get("需求者意見描述"));
               
                double emotionscore = FullTextRecordEmotionScore.getemotionscore(record.get("音檔敘述說明"));
                System.out.println(emotionscore);
                list.add(Map.of("需求者意見描述", record.get("需求者意見描述"),
                        "音檔敘述說明", record.get("音檔敘述說明"),
                        "emotionscore", emotionscore));
                //Summary關鍵字切割
                /*List<String> keywordList = TextKeyphraseExtraction.getkeywordlist(advisoryRecord.getSummary());
                for (String keyString : keywordList) {
                    SummaryKeyword summaryKeyword = new SummaryKeyword();
                    summaryKeyword.setSummaryKey_word(keyString);
                    advisoryRecord.getSummaryKeywords().add(summaryKeyword);
//                    System.out.println(keyString+":"+summaryKeyword.getSummaryKey_id());
                }
                //RequestorCommentType關鍵字切割
                List<String> TypekeywordList = TextKeyphraseExtraction.getkeywordlist(advisoryRecord.getRequestorCommentType());
                 for (String Typkey : TypekeywordList) {
                    RequestorCommentTypeKey requestorCommentTypeKey=new RequestorCommentTypeKey();
                    requestorCommentTypeKey.setRequestorCommentType_word(Typkey);
//                    System.out.println(Typkey+requestorCommentTypeKey.getRequestorCommentType_id());
                    advisoryRecord.getRequestorCommentTypeKeys().add(requestorCommentTypeKey);
                }
                advisoryRecordDao.save(advisoryRecord);*/
            }
            Gson gson=new Gson();
            String json=gson.toJson(list);
            FileUtils.write(new File("output.json"), json, "utf-8");
        }

    }
}
