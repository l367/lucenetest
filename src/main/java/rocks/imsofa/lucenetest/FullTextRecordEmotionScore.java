/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package rocks.imsofa.lucenetest;

import java.io.IOException;
import java.io.StringReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import org.apache.commons.io.IOUtils;
import com.hankcs.hanlp.restful.*;
import com.hankcs.hanlp.*;

/**
 *
 * @author lendle_win10
 */
public class FullTextRecordEmotionScore {
     public static double getemotionscore(String fulltext) throws IOException {
        Double emotionscore = 0.0;
        try {

            HanLPClient client = new HanLPClient("https://www.hanlp.com/api", "MjE5MkBiYnMuaGFubHAuY29tOjhlM1U5dFB2WnNGaHI2VGw=");
            HashMap<String, List<String>> optionhMap = new HashMap<>();
            List<String> SPK1_lines = new ArrayList<>();
            List<String> SPK2_lines = new ArrayList<>();
            optionhMap.put("SPK:1", SPK1_lines);
            optionhMap.put("SPK:2", SPK2_lines);
            String removeinfo_id = null;
            List<String> lines = IOUtils.readLines(new StringReader(fulltext));

            for (int i = 0; i < lines.size(); i++) {
                if (lines.get(i).equals("SPK:1")) {
                    optionhMap.get("SPK:1").add(lines.get(i + 2));
                } else if (lines.get(i).equals("SPK:2")) {
                    optionhMap.get("SPK:2").add(lines.get(i + 2));
                }
                if (lines.get(i).contains("貴姓") || lines.get(i).contains("稱呼")) {
                    removeinfo_id = lines.get(i - 2);
                }
            }
            HashMap<String, List<String>> origOptionhMap = new HashMap<>(optionhMap);
            if (optionhMap.size() > 1) {
                optionhMap.remove(removeinfo_id);
            }
            if (optionhMap.containsKey("SPK:1") && optionhMap.get("SPK:1").isEmpty()) {
                optionhMap.remove("SPK:1");
            }
            if (optionhMap.containsKey("SPK:2") && optionhMap.get("SPK:2").isEmpty()) {
                optionhMap.remove("SPK:2");
            }
            String remainingText = (optionhMap.isEmpty()) ? "[]" : optionhMap.get(optionhMap.keySet().toArray(new String[0])[0].toString()).toString();
            if (remainingText.length() < 10) {
                List<String> all = new ArrayList<>();
                for (List<String> value : origOptionhMap.values()) {
                    all.addAll(value);
                    all.add(" ");
                }
                remainingText = all.toString();
            }
            String SumRemainingText = HanLP.getSummary(remainingText, 500);
            if(SumRemainingText==null || SumRemainingText.trim().length()<5){
                return 0.0;
            }
//            System.out.println("SumRemainingText="+SumRemainingText);
            int testfrequency = 0;
            if (testfrequency >= 5) {
                emotionscore = 0.0;
                return emotionscore;
            } else {
                while (true && testfrequency < 5) {
                    try {
                        emotionscore = client.sentimentAnalysis(SumRemainingText);
//                        System.out.println(emotionscore + ":" + SumRemainingText);
                        break;
                    } catch (Throwable e) {
                        try {
                            System.out.println("timeout, restart......");
                            testfrequency++;
//                            e.printStackTrace();
                            Thread.sleep(5000);
                        } catch (InterruptedException ex) {
                            //Logger.getLogger(FullTextRecordEmotionScore.class.getName()).log(Level.SEVERE, null, ex);
                        }
                    }
                }
            }
            Thread.sleep(2000);

        } catch (InterruptedException ex) {
//            Logger.getLogger(FullTextRecordEmotionScore.class.getName()).log(Level.SEVERE, null, ex);
        }
        //Logger.getLogger(FullTextRecordEmotionScore.class.getName()).log(Level.INFO, "emotionscore="+emotionscore);
        return emotionscore;
    }

}
