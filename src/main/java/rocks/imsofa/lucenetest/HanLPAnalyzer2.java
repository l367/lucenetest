/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package rocks.imsofa.lucenetest;

import com.hankcs.hanlp.HanLP;
import com.hankcs.hanlp.dictionary.DynamicCustomDictionary;
import com.hankcs.lucene.HanLPAnalyzer;
import com.hankcs.lucene.HanLPTokenizer;
import java.io.File;
import java.io.IOException;
import java.util.List;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.apache.commons.io.FileUtils;
import org.apache.lucene.analysis.Tokenizer;

/**
 *
 * @author lendle
 */
public class HanLPAnalyzer2 extends HanLPAnalyzer {

    private boolean enablePorterStemming;
    private Set<String> filter;
    private DynamicCustomDictionary customDictionary = new DynamicCustomDictionary();

    /**
     * @param filter 停用词
     * @param enablePorterStemming 是否分析词干（仅限英文）
     */
    public HanLPAnalyzer2(Set<String> filter, boolean enablePorterStemming) {
        this.filter = filter;
        this.enablePorterStemming = enablePorterStemming;
        File dict = new File("dict.txt");
        try {
            List<String> lines = FileUtils.readLines(dict, "utf-8");
            for(String line : lines){
                if(line.trim().length()>1){
                    customDictionary.add(line);
                }
            }
            HanLPSetupUtil.setupCustomDictAndStopWords();
        } catch (Exception ex) {
            Logger.getLogger(HanLPAnalyzer2.class.getName()).log(Level.SEVERE, null, ex);
        }
        
    }

    /**
     * @param enablePorterStemming 是否分析词干.进行单复数,时态的转换
     */
    public HanLPAnalyzer2(boolean enablePorterStemming) {
        this.enablePorterStemming = enablePorterStemming;
    }

    public HanLPAnalyzer2() {
        super();
    }

    /**
     * 重载Analyzer接口，构造分词组件
     */
    @Override
    protected TokenStreamComponents createComponents(String fieldName) {
//        customDictionary.add("白色顆粒");
        Tokenizer tokenizer = new HanLPTokenizer(HanLP.newSegment().enableCustomDictionary(customDictionary).enableOffset(true), filter, enablePorterStemming);
        return new TokenStreamComponents(tokenizer);
    }
}
