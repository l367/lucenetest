/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Main.java to edit this template
 */
package rocks.imsofa.lucenetest;

import com.google.gson.Gson;
import java.io.File;
import java.io.FileOutputStream;
import java.io.OutputStreamWriter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVPrinter;
import org.apache.commons.io.FileUtils;

/**
 *
 * @author lendle
 */
public class ReportLogWithEmotionScore {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) throws Exception {
        // TODO code application logic here
        KeywordExtractor keywordExtractor = new KeywordExtractor();
        String json = FileUtils.readFileToString(new File("db_20231118.json"), "utf-8");
        Gson gson = new Gson();
        List records = gson.fromJson(json, List.class);
        System.out.println(records.size());
        Map<String, List<Map>> ymRecordsMap = new HashMap<>();
        for (int i = 0; i < records.size(); i++) {
            Map record = (Map) records.get(i);
            String key = record.get("date").toString().substring(0, 7);
//            System.out.println(key);
            List<Map> list = ymRecordsMap.get(key);
            if (list == null) {
                list = new ArrayList<>();
                ymRecordsMap.put(key, list);
            }
            list.add(record);
//            if(key.equals("2023-05")){
//                System.out.println(record.get("summary"));
//                System.out.println();
//            }
        }
        Map record = (Map) records.get(0);
        System.out.println(record.keySet());
        File logFile = new File("10M.csv");
        List<Map<String, String>> maps = CSV2Maps.convert(logFile, "utf-8");
        System.out.println(maps.size());
        System.out.println(maps.get(0).keySet());
        for (int i = 0; i < maps.size(); i++) {
            Map<String, String> csvRecord = maps.get(i);
            String date1 = csvRecord.get("服務日期");
            String summary1 = csvRecord.get("需求者意見描述");
            date1 = date1.substring(0, 10);
            System.out.println(date1);
            String key = date1.substring(0, 7);
            List<Map> ymCandidates = ymRecordsMap.get(key);
            boolean matched = false;
            int matchedIndex = -1;
            for (int j = 0; j < ymCandidates.size(); j++) {
                Map jsonRecord = ymCandidates.get(j);
                String date2 = (String) jsonRecord.get("date");
                date2 = date2.substring(0, 10);
                String summary2 = (String) jsonRecord.get("summary");
                if (/*date1.equals(date2) && */summary1.equals(summary2)) {
                    matched = true;
                    matchedIndex = j;
                    break;
                }
            }
            if (matched) {
//                System.out.println("\tmatched "+((Map)ymCandidates.get(matchedIndex)).get("summary"));
                System.out.println("\tmatched");
                Map matchedJsonRecord = ymCandidates.get(matchedIndex);
                csvRecord.put("情緒分數", "" + matchedJsonRecord.get("requestorCommentEmotion"));
                ymCandidates.remove(matchedIndex);
            } else {
                System.out.println("\tunmatched: " + summary1);
            }
        }
        List<String> keys = new ArrayList<>(Arrays.asList(maps.get(0).keySet().toArray(new String[0])));
        int count = 0;
        try (CSVPrinter printer = new CSVPrinter(new OutputStreamWriter(new FileOutputStream("temp.csv"), "utf-8"), CSVFormat.EXCEL)) {
            printer.printRecord(keys);
            for (Map<String, String> row : maps) {
                if (row.containsKey("情緒分數") == false) {
                    System.out.println(row.get("服務日期"));
                    continue;
                }
                List outputRow = new ArrayList();
                for (String key : keys) {
                    if (key.equals("情緒分數")) {
                        outputRow.add(Double.valueOf(row.get(key)));
                    } else {
                        outputRow.add(row.get(key));
                    }
                }
                printer.printRecord(outputRow.toArray());
                count++;
            }
        }

        Map<String, List<Double>> keyword2ScoreListMap = new HashMap<>();

        for (Map<String, String> row : maps) {
            if (row.containsKey("情緒分數") == false) {
//                    System.out.println(row.get("服務日期"));
                continue;
            }
            List<String> keywords = keywordExtractor.extractKeywords(row.get("需求者意見描述"), 10);
            for (String keyword : keywords) {
                List<Double> scores = keyword2ScoreListMap.get(keyword);
                if (scores == null) {
                    scores = new ArrayList<>();
                    keyword2ScoreListMap.put(keyword, scores);
                }
                scores.add(Double.valueOf(row.get("情緒分數")));
            }
        }
        Map<String, Double> keyword2ScoreMap = new HashMap<>();
        for (String key : keyword2ScoreListMap.keySet()) {
            List<Double> scores = keyword2ScoreListMap.get(key);
            double total = 0;
            for (double s : scores) {
                total += s;
            }
            keyword2ScoreMap.put(key, total / scores.size());
        }
        FileUtils.write(new File("keyword2ScoreMap.json"), gson.toJson(keyword2ScoreMap), "utf-8");

        List<KeywordScore> positiveKeywordScores = new ArrayList<>();
        List<KeywordScore> negativeKeywordScores = new ArrayList<>();
        for (String k : keyword2ScoreMap.keySet()) {
            double score = keyword2ScoreMap.get(k);
            if (score < 0) {
                negativeKeywordScores.add(new KeywordScore(k, score));
            } else if (score > 0) {
                positiveKeywordScores.add(new KeywordScore(k, score));
            }
        }
        Collections.sort(positiveKeywordScores, new Comparator<KeywordScore>() {
            @Override
            public int compare(KeywordScore o1, KeywordScore o2) {
                return Double.compare(o2.getScore(), o1.getScore());
            }
        });
        Collections.sort(negativeKeywordScores, new Comparator<KeywordScore>() {
            @Override
            public int compare(KeywordScore o1, KeywordScore o2) {
                return Double.compare(o1.getScore(), o2.getScore());
            }
        });

        keys.add("影響力大的關鍵字");
        try (CSVPrinter printer = new CSVPrinter(new OutputStreamWriter(new FileOutputStream("temp.csv"), "utf-8"), CSVFormat.EXCEL)) {
            printer.printRecord(keys);
            for (Map<String, String> row : maps) {
                if (row.containsKey("情緒分數") == false) {
//                    System.out.println(row.get("服務日期"));
                    continue;
                }
                List outputRow = new ArrayList();
                List<String> outputKeywords = new ArrayList<>();
                double emotionScore = -1;
                for (String key : keys) {
                    if (key.equals("情緒分數")) {
                        emotionScore = Double.valueOf(row.get(key));
                        outputRow.add(emotionScore);
                    } else if (key.equals("影響力大的關鍵字")) {
                        List<String> keywords = keywordExtractor.extractKeywords(row.get("需求者意見描述"), 20);
                        List<KeywordScore> effectiveList = (emotionScore < 0) ? negativeKeywordScores : positiveKeywordScores;
                        
                        for (KeywordScore ks : effectiveList) {
                            if (keywords.contains(ks.getKeyword())) {
                                outputKeywords.add(ks.getKeyword());
                                if (outputKeywords.size() >= 5) {
                                    break;
                                }
                            }
                        }
                        if (emotionScore == 0 || outputKeywords.size()==0) {
                            outputRow.add("-");
                        } else {
                            outputRow.add(outputKeywords.toString());
                        }
                    } else {
                        outputRow.add(row.get(key));
                    }
                }
                //if (emotionScore != 0) {
                    printer.printRecord(outputRow.toArray());
                    count++;
                //}
            }
        }
        System.out.println("total rows: " + count);
    }

}
