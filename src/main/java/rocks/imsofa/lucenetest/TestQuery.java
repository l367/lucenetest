/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Main.java to edit this template
 */
package rocks.imsofa.lucenetest;

import com.hankcs.hanlp.dictionary.CustomDictionary;
import com.hankcs.hanlp.summary.TextRankKeyword;
import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import org.apache.commons.io.FileUtils;
import org.apache.lucene.analysis.Analyzer;
import org.apache.lucene.document.Document;
import org.apache.lucene.index.DirectoryReader;
import org.apache.lucene.queryparser.classic.MultiFieldQueryParser;
import org.apache.lucene.search.IndexSearcher;
import org.apache.lucene.search.Query;
import org.apache.lucene.search.TopDocs;
import org.apache.lucene.store.Directory;
import org.apache.lucene.store.FSDirectory;

/**
 *
 * @author lendle
 */
public class TestQuery {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) throws Exception {
        // TODO code application logic here
        Path indexPath = Files.createDirectories(Path.of("index"));
        Directory directory = FSDirectory.open(indexPath);
        DirectoryReader ireader = DirectoryReader.open(directory);
        IndexSearcher isearcher = new IndexSearcher(ireader);

        Analyzer analyzer = new HanLPAnalyzer2();
        MultiFieldQueryParser multiFieldQueryParser = new MultiFieldQueryParser(new String[]{"title", "text"}, analyzer);
        
        //setup hanlp custom dictionary
        HanLPSetupUtil.setupCustomDictAndStopWords();
        /////////////////////////////////////
       
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in, "big5"));
        Pattern pattern=Pattern.compile("([\\p{IsHan}\\w]+(\\s)+){2}");
        while (true) {
            System.out.println("input query=>");
            String input=reader.readLine();
            if("exit".equals(input)){
                break;
            }
            Map<String, Float> keywordCounts=new HashMap<>();
            Query query = multiFieldQueryParser.parse(input);
            TopDocs topDocs = isearcher.search(query, 100);
            for (int i = 0; i < topDocs.scoreDocs.length; i++) {
                Document hitDoc = isearcher.doc(topDocs.scoreDocs[i].doc);
                String title=hitDoc.get("title");
                String text=hitDoc.get("text");
                Matcher m=pattern.matcher(text);
                if(m.find()){
                    text=m.replaceAll(" ");
                }
                if(text.contains("找不到網頁")){
                    continue;
                }
                System.out.println(title);
                System.out.println("\t"+text.substring(0, Math.min(50, text.length()))+":"+topDocs.scoreDocs[i].score);
//                System.out.println("\t"+text);
                List<String> keywords=TextRankKeyword.getKeywordList(text, 10);
                for(String keyword : keywords){
                    if(keyword.length()==1){
                        continue;
                    }
                    if(keywordCounts.containsKey(keyword)){
                        keywordCounts.put(keyword, keywordCounts.get(keyword)+topDocs.scoreDocs[i].score);
                    }else{
                        keywordCounts.put(keyword, topDocs.scoreDocs[i].score);
                    }
                }
            }
//            System.out.println(keywordCounts);
            float totalScore=0;
            for(float score : keywordCounts.values()){
                totalScore+=score;
            }
            List<KeywordScore> keywordScores=new ArrayList<>();
            for(String keyword : keywordCounts.keySet()){
                keywordScores.add(new KeywordScore(keyword, keywordCounts.get(keyword)/totalScore));
            }
            Collections.sort(keywordScores, new Comparator<KeywordScore>(){
                @Override
                public int compare(KeywordScore o1, KeywordScore o2) {
                    if(o1.getScore()>o2.getScore()){
                        return -1;
                    }else{
                        return 1;
                    }
                }
            });
            System.out.println(keywordScores);
        }
        ireader.close();
        directory.close();
    }

}
