/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package rocks.imsofa.lucenetest;

import com.hankcs.hanlp.HanLP;
import com.hankcs.hanlp.seg.Segment;
import com.hankcs.hanlp.seg.common.Term;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author lendle
 */
public class KeywordExtractor {
    private static Map<String, Integer> termFrequencyMap=new HashMap<>();
    static {
        try {
            //setup hanlp custom dictionary
            HanLPSetupUtil.setupCustomDictAndStopWords();
            /////////////////////////////////////
        } catch (Exception ex) {
            Logger.getLogger(KeywordExtractor.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public List<String> extractKeywords(String src, int count) throws Exception {
        //setup hanlp custom dictionary
//        HanLPSetupUtil.setupCustomDictAndStopWords();
        /////////////////////////////////////
//        List<String> ret= HanLP.extractKeyword(src, count);
        Segment segment = HanLP.newSegment().enableAllNamedEntityRecognize(true).enableCustomDictionary(true).enableCustomDictionaryForcing(true).enableIndexMode(2);
        List<Term> words = segment.seg(src);
        List<String> ret = new ArrayList<>();
        for (Term wordInfo : words) {
            if (wordInfo.word.length() >= 2 && wordInfo.word.contains(",") == false && HanLPSetupUtil.dictionary.containsKey(wordInfo.word)) {
//                        System.out.println(wordInfo.getFrequency()+":"+wordInfo.word);
                termFrequencyMap.put(wordInfo.word, wordInfo.getFrequency());
                ret.add(wordInfo.word);
            }
        }
        ret = HanLP.extractKeyword(String.join(",", ret), count);
        //remove substrings
        /*List<String> longest=new ArrayList<>();
        for(String s : ret){
            for(String s1 : ret){
                if(!(s.length()<s1.length() && s1.contains(s))){
                    longest.add(s);
                }else{
                    System.out.println("skip "+s);
                }
            }
        }*/
        return ret;
    }

    public List<String> extractPhrases(String src, int count) throws Exception {
        Segment segment = HanLP.newSegment().enableAllNamedEntityRecognize(true).enableCustomDictionary(true).enableCustomDictionaryForcing(true).enableIndexMode(2);
        List<Term> words = segment.seg(src);
        List<String> ret = new ArrayList<>();
        for (Term wordInfo : words) {
            if (wordInfo.word.length() >= 2 && wordInfo.word.contains(",") == false) {
//                        System.out.println(wordInfo.getFrequency()+":"+wordInfo.word);
                termFrequencyMap.put(wordInfo.word, wordInfo.getFrequency());
                ret.add(wordInfo.word);
            }
        }
        ret = HanLP.extractPhrase(String.join(",", ret), count);
        return ret;
    }
    
    public static int getTermFrequency(String term){
        if(termFrequencyMap.containsKey(term)){
            int freq=termFrequencyMap.get(term);
            return Math.max(freq, 1);
        }else{
            return 1;
        }
    }
}
