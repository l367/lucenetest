/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package rocks.imsofa.lucenetest;

/**
 *
 * @author lendle
 */
public class KeywordScore {
    private String keyword=null;
    private double score=0;

    public KeywordScore(String keyword, double score) {
        this.keyword=keyword;
        this.score=score;
    }

    public String getKeyword() {
        return keyword;
    }

    public void setKeyword(String keyword) {
        this.keyword = keyword;
    }

    public double getScore() {
        return score;
    }

    public void setScore(double score) {
        this.score = score;
    }

    @Override
    public String toString() {
        return "KeywordScore{" + "keyword=" + keyword + ", score=" + score + '}';
    }
    
    
}
