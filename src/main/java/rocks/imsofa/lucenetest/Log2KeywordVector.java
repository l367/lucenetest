/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Main.java to edit this template
 */
package rocks.imsofa.lucenetest;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.io.Reader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVRecord;

/**
 *
 * @author lendle
 */
public class Log2KeywordVector {
    private static KeywordExtractor keywordExtractor=new KeywordExtractor();
    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) throws Exception {
        // TODO code application logic here
        File logFile1 = new File("111馬辦來電資料.csv");
//        File logFile2 = new File("1-7m(去識別).csv");
        File logFile2 = new File("10M.csv");
        File indicatorDataFile=new File("economic_indicator.csv");
        Map<String, Integer> key2ClusterMap = new HashMap<>();
        File keyordClusterMapCSV = new File("co_occurrence_matrix_cluster.csv");
        try (Reader in = new InputStreamReader(new FileInputStream(keyordClusterMapCSV), "utf-8")) {
            Iterable<CSVRecord> records = CSVFormat.Builder.create(CSVFormat.EXCEL).setHeader().build().parse(in);
            for (CSVRecord record : records) {
                String word=record.get("X");
                int cluster=Integer.valueOf(record.get("cluster"));
                key2ClusterMap.put(word, cluster);
            }
        }
        IndicatorData indicatorData=IndicatorData.loadFromCSV(indicatorDataFile, "utf-8");
        List<Record> list1=toRecords(key2ClusterMap, logFile1, indicatorData);
        List<Record> list2=toRecords(key2ClusterMap, logFile2, indicatorData);
        list1.addAll(list2);
        try(PrintWriter writer=new PrintWriter(new OutputStreamWriter(new FileOutputStream("indicator_analysis.csv"), "utf-8" ))){
            writer.println("year,month,signal,v1,v2,v3,v4,v5,value,delta,majorType,minorType");
            for(Record record : list1){
                String str=String.format("%d,%d,%f,%d,%d,%d,%d,%d,%f,%f,%s,%s", record.year,record.month,record.signalValue,record.vector[0],
                        record.vector[1],record.vector[2],record.vector[3],record.vector[4],
                        record.indicatorValue, record.indicatorDelta,
                        "\""+record.getMajorType()+"\"", "\""+record.getMinorType()+"\""
                );
                writer.println(str);
            }
        }
    }
    
    private static List<Record> toRecords(Map<String, Integer> key2ClusterMap, File logFile, IndicatorData indicatorData) throws Exception{
        List<Record> ret = new ArrayList<>();
        List<Map<String, String>> maps = CSV2Maps.convert(logFile, "utf-8");
        for (Map<String, String> record : maps) {
            Record logRecord = new Record();
            String comment = record.get("需求者意見描述");
            logRecord.setComment(comment);
            List<String> keywords = keywordExtractor.extractKeywords(comment, 50);
            for (String keyword : keywords) {
                if (key2ClusterMap.containsKey(keyword)) {
                    int cluster = key2ClusterMap.get(keyword);
                    logRecord.getVector()[cluster - 1]++;
                }
            }
            if (record.containsKey("月份")) {
                logRecord.setYear(2022);
                logRecord.setMonth(Integer.valueOf(record.get("月份").replace("月", "")));

            } else {
                logRecord.setYear(2023);
                logRecord.setMonth(Integer.valueOf(record.get("分析---月份").replace("月", "")));
            }
            
            if(logRecord.getYear()==2023 && logRecord.getMonth()==10){
                break;
            }
            
            logRecord.setIndicatorValue(indicatorData.getValue(logRecord.getYear(), logRecord.getMonth()));
            logRecord.setIndicatorDelta(indicatorData.getDelta(logRecord.getYear(), logRecord.getMonth()));
            logRecord.setSignalValue(indicatorData.getSignal(logRecord.getYear(), logRecord.getMonth()));
            logRecord.setMajorType(record.get("問題類別大項"));
            logRecord.setMinorType(record.get("需求者意見選項"));
            ret.add(logRecord);
        }
        
        return ret;
    }
    
    static class Record{
        private int year=-1;
        private int month=-1;
        private String comment=null;
        private int [] vector=new int[5];
        private double indicatorValue=-1;
        private double indicatorDelta=-1;
        private double signalValue=-1;
        private String majorType=null;
        private String minorType=null;

        public String getMajorType() {
            return majorType;
        }

        public void setMajorType(String majorType) {
            this.majorType = majorType;
        }

        public String getMinorType() {
            return minorType;
        }

        public void setMinorType(String minorType) {
            this.minorType = minorType;
        }
        
        

        public int getYear() {
            return year;
        }

        public void setYear(int year) {
            this.year = year;
        }

        public int getMonth() {
            return month;
        }

        public void setMonth(int month) {
            this.month = month;
        }

        public String getComment() {
            return comment;
        }

        public void setComment(String comment) {
            this.comment = comment;
        }

        public int[] getVector() {
            return vector;
        }

        public double getIndicatorValue() {
            return indicatorValue;
        }

        public void setIndicatorValue(double indicatorValue) {
            this.indicatorValue = indicatorValue;
        }

        public double getIndicatorDelta() {
            return indicatorDelta;
        }

        public void setIndicatorDelta(double indicatorDelta) {
            this.indicatorDelta = indicatorDelta;
        }

        @Override
        public String toString() {
            return "Record{" + "year=" + year + ", month=" + month + ", comment=" + comment + ", vector=" + vector + ", indicatorValue=" + indicatorValue + ", indicatorDelta=" + indicatorDelta + '}';
        }

        public double getSignalValue() {
            return signalValue;
        }

        public void setSignalValue(double signalValue) {
            this.signalValue = signalValue;
        }
        
        
        
    }
    
    static class IndicatorData{
        private Map<String, Double> valueMap=new HashMap<>();
        private Map<String, Double> deltaMap=new HashMap<>();
        private Map<String, Double> signalMap=new HashMap<>();
        
        public static IndicatorData loadFromCSV(File csvFile, String charSet) throws Exception{
            IndicatorData data=new IndicatorData();
            List<Map<String, String>> rows=CSV2Maps.convert(csvFile, charSet);
            for(Map<String, String> row : rows){
//                System.out.println(row);
//                System.out.println(row.get("月份")+", "+row.get("領先指標"));
                data.valueMap.put(row.get("月份"), Double.valueOf(row.get("領先指標")));
                data.deltaMap.put(row.get("月份"), Double.valueOf(row.get("delta")));
                data.signalMap.put(row.get("月份"), Double.valueOf(row.get("景氣對策信號")));
            }
            return data;
        }
        
        public double getValue(int year, int month){
            String key="";
            if(month<10){
                key=""+year+"-0"+month;
            }else{
                key=""+year+"-"+month;
            }
            System.out.println("key="+key);
            return valueMap.get(key);
        }
        
        public double getDelta(int year, int month){
            String key="";
            if(month<10){
                key=""+year+"-0"+month;
            }else{
                key=""+year+"-"+month;
            }
            return deltaMap.get(key);
        }
        
        public double getSignal(int year, int month){
            String key="";
            if(month<10){
                key=""+year+"-0"+month;
            }else{
                key=""+year+"-"+month;
            }
            return signalMap.get(key);
        }
    }
}
