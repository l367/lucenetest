/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Main.java to edit this template
 */
package rocks.imsofa.lucenetest;

import com.hankcs.hanlp.restful.HanLPClient;
import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import org.apache.commons.io.FileUtils;

/**
 *
 * @author USER
 */
public class CreateDict {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) throws Exception {
        // TODO code application logic here
        File dir = new File("indexing");
        Map<String, String> allTokens = new HashMap<>();
        Map<String, String> stopWords = new HashMap<>();
        List<String> lines = FileUtils.readLines(new File("stopwords.txt"), "utf-8");
        for (String line : lines) {
            stopWords.put(line, "");
        }
        for (File file : dir.listFiles()) {
            try {
//            System.out.println("processing start=" + file);
                String text = FileUtils.readFileToString(file, "utf-8");
                HanLPClient client = new HanLPClient("https://www.hanlp.com/api", "MjE5MkBiYnMuaGFubHAuY29tOjhlM1U5dFB2WnNGaHI2VGw=");
                List<String> tokens = new ArrayList<>();
                int start = 0;
                int end = Math.min(text.length(), start + 1000);
                String current = text.substring(start, end);
                while (current != null) {
                    List<List<String>> ret = client.tokenize(current);
                    for (List<String> seg : ret) {
                        tokens.addAll(seg);
                    }
                    String lastToken = tokens.get(tokens.size() - 1);
                    int lastIndex = current.lastIndexOf(lastToken);

                    start = lastIndex + start - 1;
                    end = Math.min(text.length(), start + 1000);
                    if (start >= text.length() || ret.size() == 1) {
                        current = null;
                    } else {
                        current = text.substring(start, end);
                        tokens.remove(tokens.size() - 1);
                    }
                    Thread.sleep(2000);
                }
                StringBuilder sb = new StringBuilder();
                for (String t : tokens) {
                    if (t.length() > 1 && stopWords.containsKey(t) == false && allTokens.containsKey(t) == false && t.length() < 10) {
                        allTokens.put(t, "");
                        sb.append(t).append("\r\n");
                    }
                }

                FileUtils.write(new File("dict.txt"), sb.toString(), "utf-8", true);
            } catch (Throwable e) {
                System.out.println("error, skip " + file);
                Thread.sleep(30000);
            }
        }

    }

}
