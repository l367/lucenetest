/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Main.java to edit this template
 */
package rocks.imsofa.lucenetest;

import com.github.kevinsawicki.http.HttpRequest;
import com.google.gson.Gson;
import com.hankcs.hanlp.HanLP;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVRecord;
import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.StringEscapeUtils;

/**
 *
 * @author lendle
 */
public class KeywordsFromCommentsLog {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) throws Exception {
        // TODO code application logic here
        File logFile = new File("test.csv");
        Map<String, String> keyMap = new HashMap<>();
        Map<String, String> processedMap=new HashMap<>();
        try (Reader in = new InputStreamReader(new FileInputStream(logFile), "big5")) {
            Iterable<CSVRecord> records = CSVFormat.Builder.create(CSVFormat.EXCEL).setHeader().build().parse(in);
            for (CSVRecord record : records) {
                String comment = record.get("音檔敘述說明");
                List<String> _keywords = HanLP.extractKeyword(comment, 10);
                for (String key : _keywords) {
                    if(key.length()<2 || "SPK".equals(key)){
                        continue;
                    }
                    if(processedMap.containsKey(key)){
                        continue;
                    }
                    processedMap.put(key, "");
                    try {
                        Thread.sleep(2000);
                        String src = HttpRequest.get("https://customsearch.googleapis.com/customsearch/v1?cx=967d66347cb334550&key=AIzaSyCG_vwPBCAp1DZZogsuP0-yCJWBAQDxZVY&lr=lang_zh-TW&sort=date:r:20230101:20230131&start=" + 1 + "&q=" + key).body();
                        Gson gson = new Gson();
                        Map topMap = gson.fromJson(src, Map.class);
                        List<Map> items = (List) topMap.get("items");

                        if (items != null) {
                            for (Map item : items) {
                                String htmlSnippet = (String) item.get("htmlSnippet");
                                if (htmlSnippet == null) {
                                    continue;
                                }

                                htmlSnippet = StringEscapeUtils.unescapeJava(htmlSnippet);
                                Pattern p = Pattern.compile("<b>[^<]+</b>");
                                Matcher m = p.matcher(htmlSnippet);
                                while (m.find()) {
                                    String group = m.group();
                                    System.out.println(group);
                                    int index1 = group.indexOf(">");
                                    int index2 = group.indexOf("<", index1 + 1);
                                    group = group.substring(index1 + 1, index2);
                                    if (group.equals("...")) {
                                        continue;
                                    }
                                    keyMap.put(group, "");
                                }
                                if (item.containsKey("pagemap")) {
                                    Map map = (Map) item.get("pagemap");
                                    if (map.containsKey("website")) {
                                        List<Map> webSites = (List) map.get("website");
                                        for (Map map1 : webSites) {
                                            if (map1.containsKey("keywords")) {
                                                String keywords = (String) map1.get("keywords");
                                                String[] keywordsArray = keywords.split("(\\s)*,(\\s)*");
                                                for (String k : keywordsArray) {
                                                    keyMap.put(k, "");
                                                }
                                            }
                                        }
                                    }
                                }

                            }
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }
        }
        if (keyMap.size() > 0) {
            System.out.println(keyMap.keySet());
        }
        String newDictString = String.join("\r\n", keyMap.keySet());
        FileUtils.write(new File("commentsdict.txt"), newDictString, "utf-8");
    }

}
