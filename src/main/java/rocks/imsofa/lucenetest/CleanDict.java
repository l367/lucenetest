/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Main.java to edit this template
 */
package rocks.imsofa.lucenetest;

import java.io.File;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import org.apache.commons.io.FileUtils;

/**
 *
 * @author lendle
 */
public class CleanDict {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) throws Exception {
        // TODO code application logic here
        Set<String> set=new HashSet<>();
        List<String> lines=FileUtils.readLines(new File("dict.txt"), "utf-8");
        for(String line : lines){
            if(line.length()>30){
//                System.out.println(line);
            }else if(line.matches("[\\d\\.]+")){
//                System.out.println(line);
            }else{
                set.add(line);
            }
        }
        System.out.println("check "+set.size()+" keywords");
        Set<String> result=new HashSet<>();
        int count=0;
        for(String key : set){
            //check against wikipedia
            try{
                if(CorrectKeywordWithWikipedia.check(key)){
                    System.out.println(key+":ok");
                    result.add(key);
                }else{
                    System.out.println(key+":error");
                }
            }catch(Throwable e){}
            count++;
            Thread.sleep(1000);
            if(count%10==0){
                Thread.sleep(10000);
            }
        }
        for(String key : result){
            FileUtils.write(new File("dict1.txt"), key+"\r\n", "utf-8", true);
        }
    }
    
}
