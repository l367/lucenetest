/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 */
package rocks.imsofa.lucenetest;

import com.google.gson.Gson;
import java.io.File;
import java.io.FileReader;
import java.io.Reader;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import org.apache.commons.csv.*;
import org.apache.commons.io.FileUtils;
import com.hankcs.hanlp.*;
import com.hankcs.hanlp.dictionary.CustomDictionary;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.HashMap;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author lendle_win10
 */
public class TrainStopWords {

    public static void main(String[] args) throws Exception {
        File logFile = new File("test.csv");
        File csvFile = CSVFormatProcessing.tocsvformat(logFile);
        HanLPSetupUtil.setupCustomDictAndStopWords();
        Map<String, String> keywordsMap = new HashMap<>();
        Map<String, String> stopWords = new HashMap<>();
        File dict = new File("dict.txt");
        try {
            List<String> lines = FileUtils.readLines(dict, "utf-8");
            for (String line : lines) {
                if (line.trim().length() > 1) {
                    keywordsMap.put(line, "");
                }
            }
        } catch (IOException ex) {
            //Logger.getLogger(HanLPSetupUtil.class.getName()).log(Level.SEVERE, null, ex);
        }
        try (Reader in = new InputStreamReader(new FileInputStream(csvFile), "big5")) {
            Iterable<CSVRecord> records = CSVFormat.Builder.create(CSVFormat.EXCEL).setHeader().build().parse(in);
            for (CSVRecord record : records) {
                /*if (record.get("音檔敘述說明") == null) {
                    continue;
                }*/

                //System.out.println(record.get("需求者意見選項"));
                //System.out.println(record.get("需求者意見描述"));
                String str = record.get("需求者意見描述");
                
                for (String k : keywordsMap.keySet()) {
                    if (keywordsMap.containsKey(k)) {
                        if(str.indexOf(k)==-1){
                            continue;
                        }
                        int from = 0;
                        while (from < str.length()) {
                            int index = str.indexOf(str, from);
                            int startIndex = -1;
                            int gram2Index = -1;
                            if (index != -1) {
                                startIndex = index + k.length();
                                gram2Index = startIndex + 2;
                                if (gram2Index < str.length()) {
                                    String s = str.substring(startIndex, gram2Index);
                                    s=s.trim();
                                    if(s.length()==0){
                                        continue;
                                    }
                                    char c=s.charAt(0);
                                    if(c=='(' || c==')' || c=='【' || c=='「'){
                                        stopWords.put(""+c, "");
                                    }else if (keywordsMap.containsKey(s) == false) {
                                        stopWords.put(s, "");
                                    }
                                }
                            }
                            from++;
                        }

                    }
                }
//                System.out.println(keywords);
            }

        }
        for(String s : stopWords.keySet()){
            System.out.println(s);
        }
    }
}
