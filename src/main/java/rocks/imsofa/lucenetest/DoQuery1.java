/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Main.java to edit this template
 */
package rocks.imsofa.lucenetest;

import com.hankcs.lucene.HanLPAnalyzer;
import java.nio.file.Files;
import java.nio.file.Path;
import org.apache.lucene.analysis.Analyzer;
import org.apache.lucene.document.Document;
import org.apache.lucene.index.DirectoryReader;
import org.apache.lucene.index.Term;
import org.apache.lucene.search.IndexSearcher;
import org.apache.lucene.search.MultiPhraseQuery;
import org.apache.lucene.search.Query;
import org.apache.lucene.search.TopDocs;
import org.apache.lucene.store.Directory;
import org.apache.lucene.store.FSDirectory;

/**
 *
 * @author USER
 */
public class DoQuery1 {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) throws Exception {
        // TODO code application logic here
        Path indexPath = Files.createDirectories(Path.of("index2"));
        Directory directory = FSDirectory.open(indexPath);
        DirectoryReader ireader = DirectoryReader.open(directory);
        IndexSearcher isearcher = new IndexSearcher(ireader);

        Analyzer analyzer = new HanLPAnalyzer2();
        MultiPhraseQuery.Builder mBuilder = new MultiPhraseQuery.Builder();
        Query query = mBuilder.add(new Term("text", "白色顆粒")).build();
//        FuzzyQuery query=new FuzzyQuery(new Term("text", "紓困"));
        TopDocs topDocs = isearcher.search(query, 10);
        for (int i = 0; i < topDocs.scoreDocs.length; i++) {
            Document hitDoc = isearcher.doc(topDocs.scoreDocs[i].doc);
            System.out.println(hitDoc.get("text"));
        }
        ireader.close();
        directory.close();
    }

}
