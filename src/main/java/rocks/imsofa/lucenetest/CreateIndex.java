/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Main.java to edit this template
 */
package rocks.imsofa.lucenetest;

import com.hankcs.lucene.HanLPAnalyzer;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.List;
import org.apache.commons.io.FileUtils;
import org.apache.lucene.analysis.Analyzer;
import org.apache.lucene.analysis.standard.StandardAnalyzer;
import org.apache.lucene.document.Document;
import org.apache.lucene.document.Field;
import org.apache.lucene.document.StringField;
import org.apache.lucene.document.TextField;
import org.apache.lucene.index.IndexWriter;
import org.apache.lucene.index.IndexWriterConfig;
import org.apache.lucene.store.Directory;
import org.apache.lucene.store.FSDirectory;

/**
 *
 * @author USER
 */
public class CreateIndex {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) throws Exception {
        // TODO code application logic here
        HanLPAnalyzer2 analyzer = new HanLPAnalyzer2();
        HanLPSetupUtil.setupCustomDictAndStopWords();
        Path indexPath = Files.createDirectories(Path.of("index"));
        Directory directory = FSDirectory.open(indexPath);
        IndexWriterConfig config = new IndexWriterConfig(analyzer);
        config.setOpenMode(IndexWriterConfig.OpenMode.CREATE_OR_APPEND);
        IndexWriter iwriter = new IndexWriter(directory, config);

        File indexingDir = new File("indexing");
        for (File subFolder : indexingDir.listFiles()) {
            System.out.println(subFolder);
            indexFolder(subFolder, iwriter);
            /*for (File file : subFolder.listFiles()) {
                if (file.getName().equals(".hashtable.json") == true) {
                    continue;
                }
                Document doc = new Document();
                List<String> lines = FileUtils.readLines(file, "utf-8");
                String title = lines.remove(0);
                String date = lines.remove(0).replace("-", "");
                String link = lines.remove(0);
                String text = String.join("\r\n", lines.toArray(new String[0]));
                if (link.endsWith(".pdf")) {
                    System.out.println("skip: " + file.getName() + "\r\n\t" + title);
                    continue;
                }
                doc.add(new TextField("id", "" + System.currentTimeMillis(), Field.Store.YES));
                doc.add(new TextField("title", title, Field.Store.YES));
                doc.add(new StringField("link", link, Field.Store.YES));
                doc.add(new StringField("date", date, Field.Store.YES));
                doc.add(new TextField("text", text, Field.Store.YES));
                iwriter.addDocument(doc);
            }*/
        }
        iwriter.close();
    }

    static void indexFolder(File subFolder, IndexWriter iwriter) throws IOException {
        for (File file : subFolder.listFiles()) {
            if (file.isDirectory()) {
                indexFolder(file, iwriter);
            } else {
                if (file.getName().equals(".hashtable.json") == true) {
                    continue;
                }
                System.out.println("\t"+file);
                Document doc = new Document();
                List<String> lines = FileUtils.readLines(file, "utf-8");
                String title = lines.remove(0);
                String date = lines.remove(0).replace("-", "");
                String link = lines.remove(0);
                String text = String.join("\r\n", lines.toArray(new String[0]));
                if (link.endsWith(".pdf")) {
                    System.out.println("skip: " + file.getName() + "\r\n\t" + title);
                    continue;
                }
                doc.add(new TextField("id", "" + System.currentTimeMillis(), Field.Store.YES));
                doc.add(new TextField("title", title, Field.Store.YES));
                doc.add(new StringField("link", link, Field.Store.YES));
                doc.add(new StringField("date", date, Field.Store.YES));
                doc.add(new TextField("text", text, Field.Store.YES));
                iwriter.addDocument(doc);
            }
        }
    }

}
