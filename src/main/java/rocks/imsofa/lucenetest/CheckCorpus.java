/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Main.java to edit this template
 */
package rocks.imsofa.lucenetest;

import java.io.File;
import org.apache.commons.io.FileUtils;

/**
 *
 * @author lendle
 */
public class CheckCorpus {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) throws Exception{
        // TODO code application logic here
        File folder=new File("indexing");
        for(File file : folder.listFiles()){
            String title=FileUtils.readLines(file, "utf-8").get(0);
            System.out.println(title);
        }
    }
    
}
