/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Main.java to edit this template
 */
package rocks.imsofa.lucenetest;

import com.hankcs.hanlp.restful.HanLPClient;
import java.io.File;
import java.io.FileReader;
import java.io.Reader;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVRecord;
import org.apache.commons.io.FileUtils;

/**
 *
 * @author USER
 */
public class CreateBaseDict {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) throws Exception {
        // TODO code application logic here
        File dir = new File("indexing");
        Map<String, String> allTokens = new HashMap<>();
//        Map<String, String> stopWords = new HashMap<>();
//        List<String> lines = FileUtils.readLines(new File("stopwords.txt"), "utf-8");
//        for (String line : lines) {
//            stopWords.put(line, "");
//        }
        StringBuilder buffer = new StringBuilder();
        for (File file : dir.listFiles()) {
            try {
//            System.out.println("processing start=" + file);
                String text = FileUtils.readLines(file, "utf-8").get(0);
                buffer.append(text).append("\r\n");
                if (buffer.length() > 800) {
                    HanLPClient client = new HanLPClient("https://www.hanlp.com/api", "MjE5MkBiYnMuaGFubHAuY29tOjhlM1U5dFB2WnNGaHI2VGw=");
//                    List<String> keywords = new ArrayList<>(client.keyphraseExtraction(buffer.toString()).keySet());
                    List<String> keywords = client.tokenize(text).get(0);

                    StringBuilder sb = new StringBuilder();
                    for (String t : keywords) {
                        if (allTokens.containsKey(t) == false && t.length() > 1 && t.length() < 10) {
                            allTokens.put(t, "");
                            sb.append(t).append("\r\n");
                        }
                    }
                    buffer = new StringBuilder();
                    System.out.println(sb.toString());
                    FileUtils.write(new File("base_dict.txt"), sb.toString(), "utf-8", true);
                    Thread.sleep(2000);
                }
            } catch (Throwable e) {
                System.out.println("error, skip " + file);
                Thread.sleep(30000);
            }
        }
        if (buffer.length() > 0) {
            String text=buffer.toString();
            HanLPClient client = new HanLPClient("https://www.hanlp.com/api", "MjE5MkBiYnMuaGFubHAuY29tOjhlM1U5dFB2WnNGaHI2VGw=");
            Map<String, Double> keywords = client.keyphraseExtraction(text);

            StringBuilder sb = new StringBuilder();
            for (String t : keywords.keySet()) {
                if (allTokens.containsKey(t) == false && t.length() > 1 && t.length() < 10) {
                    allTokens.put(t, "");
                    sb.append(t).append("\r\n");
                }
            }
            buffer = new StringBuilder();
            FileUtils.write(new File("base_dict.txt"), sb.toString(), "utf-8", true);
        }
        
        Reader in = new FileReader("112_1_2.csv");
        Set<String> options = new HashSet<>();
        Iterable<CSVRecord> records = CSVFormat.Builder.create(CSVFormat.EXCEL).setHeader().build().parse(in);
        HanLPClient client = new HanLPClient("https://www.hanlp.com/api", "MjE5MkBiYnMuaGFubHAuY29tOjhlM1U5dFB2WnNGaHI2VGw=");
        Set<String> keyOptions = new HashSet<>();
        for (CSVRecord record : records) {
            String option = record.get("需求者意見選項");
            options.add(option);
        }
        for (String option : options) {
            List<String> keyPhrases = client.tokenize(option).get(0);
            for(String keyString : keyPhrases){
                FileUtils.write(new File("base_dict.txt"), keyString+"\r\n", "utf-8", true);
            }

            Thread.sleep(1000);
        }
        System.out.println(keyOptions);

    }

}
