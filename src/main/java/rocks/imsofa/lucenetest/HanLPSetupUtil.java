/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package rocks.imsofa.lucenetest;

import com.hankcs.hanlp.dictionary.CustomDictionary;
import com.hankcs.hanlp.dictionary.stopword.CoreStopWordDictionary;
import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.apache.commons.io.FileUtils;

/**
 *
 * @author lendle
 */
public class HanLPSetupUtil {
    private static boolean initialized=false;
    public static Map<String, String> dictionary=new HashMap<>();
    public synchronized static void setupCustomDictAndStopWords() throws Exception{
        if(initialized){
            return;
        }
        File dict = new File("newdict.txt");
        try {
            System.out.println("dict loaded");
            List<String> lines = FileUtils.readLines(dict, "utf-8");
            for (String line : lines) {
                line=line.trim();
                if (line.length() > 1 && line.length()<50) {
                    CustomDictionary.add(line);
                    dictionary.put(line, "");
                }
            }
        } catch (IOException ex) {
            Logger.getLogger(HanLPSetupUtil.class.getName()).log(Level.SEVERE, null, ex);
        }
        File stopWords = new File("stopwords.txt");
        try {
            List<String> lines = FileUtils.readLines(stopWords, "utf-8");
            for (String line : lines) {
                CoreStopWordDictionary.add(line);
            }
        } catch (IOException ex) {
            Logger.getLogger(HanLPAnalyzer2.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}
