/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Main.java to edit this template
 */
package rocks.imsofa.lucenetest;

import com.hankcs.hanlp.HanLP;
import com.hankcs.hanlp.seg.common.Term;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVRecord;

/**
 *
 * @author lendle
 */
public class ProcessSummaryVerb2 {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) throws Exception {
        // TODO code application logic here
        //setup hanlp custom dictionary
//        HanLPSetupUtil.setupCustomDictAndStopWords();
        /////////////////////////////////////
//        File sourceFile=new File("3M-ALL_TEXT (元智團隊).xlsx");
        File sourceFile = new File("1-7m(去識別).csv");
        List<String> complaining=List.of("糟蹋", "敷衍", "抱怨");
        List<String> arguing=List.of("不能接受", "不肯", "不想", "婉拒", "疑惑", "抵制", "回絕");
        List<String> attacking=List.of("糟蹋", "羞辱");
        try (Reader in = new InputStreamReader(new FileInputStream(sourceFile), "utf-8")) {
            Iterable<CSVRecord> records = CSVFormat.Builder.create(CSVFormat.EXCEL).setHeader().build().parse(in);
            for (CSVRecord record : records) {
                String content = (record.get("需求者意見描述"));
                
                List<Term> terms = HanLP.segment(content);
                int complainingScore=0;
                int arguingScore=0;
                int attackingScore=0;
                for (Term term : terms) {
                    if(complaining.contains(term.word)){
                        complainingScore++;
                    }else if(arguing.contains(term.word)){
                        arguingScore++;
                    }else if(attacking.contains(term.word)){
                        attackingScore++;
                    }
                }
                if((complainingScore+arguingScore+attackingScore)>0){
                    System.out.println(String.format("complaining=%d, arguing=%d, attacking=%d", complainingScore, arguingScore, attackingScore));
                    System.out.println("\t"+content);
                }
            }
        }
//        System.out.println(count);
    }

}
