/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Main.java to edit this template
 */
package rocks.imsofa.lucenetest;

import com.hankcs.hanlp.HanLP;
import com.hankcs.hanlp.summary.TextRankKeyword;
import java.io.File;
import java.io.FileReader;
import java.io.Reader;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVRecord;
import org.apache.commons.io.FileUtils;

/**
 *
 * @author lendle
 */
public class CreateKeywordTable {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) throws Exception {
        // TODO code application logic here
        //setup hanlp custom dictionary
        HanLPSetupUtil.setupCustomDictAndStopWords();
        /////////////////////////////////////
//        File sourceFile=new File("3M-ALL_TEXT (元智團隊).xlsx");
        File sourceFile=new File("4M.xlsx");
        File tempFolder=new File(".temp");
        File outputFile=new File("keyword_output.csv");
        tempFolder.mkdirs();
        FileUtils.copyFileToDirectory(sourceFile, tempFolder);
        sourceFile=new File(tempFolder, sourceFile.getName());
        File csvFile=CSVFormatProcessing.tocsvformat(sourceFile);
//        Map<String, Integer> count=new HashMap<>();
        FileUtils.write(outputFile, "受訪者性別,是否重複來電,來源,關鍵字", "utf-8", false);
        try(Reader in = new FileReader(csvFile)){
            Iterable<CSVRecord> records = CSVFormat.Builder.create(CSVFormat.EXCEL).setHeader().build().parse(in);
            for (CSVRecord record : records) {
                String month=record.get("分析---月份");
                if("4月".equals(month)==false){
                    continue;
                }
                String sex=(record.get("受訪者性別"));
                boolean duplicated="是".equals((record.get("分析--是否重複來電")));
                String source=(record.get("分析--來源"));
                String content=(record.get("需求者意見描述"));
                //List<String> list=TextRankKeyword.getKeywordList(content, 10);
                List<String> list=HanLP.extractKeyword(content, 10);
                for(String str : list){
                    if(str.length()<=1){
                        continue;
                    }
                    FileUtils.write(outputFile, "\r\n", "utf-8", true);
                    FileUtils.write(outputFile, sex+","+duplicated+","+source+","+str, "utf-8", true);
//                    if(count.containsKey(str)){
//                        count.put(str, count.get(str)+1);
//                    }else{
//                        count.put(str, 1);
//                    }
                }
            }
        }
//        System.out.println(count);
    }
    
}
