/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package rocks.imsofa.lucenetest;

import java.io.File;
import java.io.FileInputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVRecord;

/**
 *
 * @author USER
 */
public class CSV2Maps {
    public static List<Map<String, String>> convert(File csvFile, String charSet) throws Exception{
        List<Map<String, String>> ret=new ArrayList<>();
        try (Reader in = new InputStreamReader(new FileInputStream(csvFile), charSet)) {
            Iterable<CSVRecord> records = CSVFormat.Builder.create(CSVFormat.EXCEL).setHeader().build().parse(in);
            for (CSVRecord record : records) {
                ret.add(record.toMap());
            }
        }
        return ret;
    }
}
