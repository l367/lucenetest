/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Main.java to edit this template
 */
package rocks.imsofa.lucenetest;

import com.github.kevinsawicki.http.HttpRequest;
import com.hankcs.hanlp.HanLP;
import java.io.File;
import java.net.URLEncoder;
import java.util.List;
import org.apache.commons.io.FileUtils;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.select.Elements;

/**
 *
 * @author USER
 */
public class CorrectKeywordWithWikipedia {

    public static boolean check(String keyword) throws Exception {
        String src = HttpRequest.get("https://zh.wikipedia.org/w/index.php?search=" + URLEncoder.encode(keyword, "utf-8") + "&title=Special:%E6%90%9C%E7%B4%A2&profile=advanced&fulltext=1&ns0=1").body();
        Document doc = Jsoup.parse(src);
        Elements elements = doc.select(".mw-search-createlink");
        if (elements == null || elements.isEmpty()) {
            return true;
        } else {
            StringBuilder sb = new StringBuilder();
            elements = doc.select(".searchmatch");
            for (int i = 0; i < elements.size(); i++) {
                if (elements.get(i).text().length() <= 1) {
                    continue;
                }
                sb.append(elements.get(i).text());
            }
//                System.out.println(sb);
            if (sb.toString().contains(keyword)) {
                return true;
            } else {
                return false;
            }
        }
    }

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) throws Exception {
        // TODO code application logic here
        File dict = new File("newdict.txt");
        List<String> lines = FileUtils.readLines(dict, "utf-8");
        for (String line : lines) {
            Thread.sleep(1000);
            String keyword = line;
            String src = HttpRequest.get("https://zh.wikipedia.org/w/index.php?search=" + URLEncoder.encode(keyword, "utf-8") + "&title=Special:%E6%90%9C%E7%B4%A2&profile=advanced&fulltext=1&ns0=1").body();
            Document doc = Jsoup.parse(src);
            Elements elements = doc.select(".mw-search-createlink");
            if (elements == null || elements.isEmpty()) {
                //the keyword must exists
                System.out.println(keyword + ":yes");
            } else {
                StringBuilder sb = new StringBuilder();
                elements = doc.select(".searchmatch");
                for (int i = 0; i < elements.size(); i++) {
                    if (elements.get(i).text().length() <= 1) {
                        continue;
                    }
                    sb.append(elements.get(i).text());
                }
//                System.out.println(sb);
                if (sb.toString().contains(keyword)) {
                    System.out.println(keyword + ":yes");
                } else {
                    System.out.println(keyword + ":no");
                }
            }
        }
    }

}
