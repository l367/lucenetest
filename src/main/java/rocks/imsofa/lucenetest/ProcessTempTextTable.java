/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Main.java to edit this template
 */
package rocks.imsofa.lucenetest;

import java.io.File;
import java.util.List;
import org.apache.commons.io.FileUtils;

/**
 *
 * @author lendle
 */
public class ProcessTempTextTable {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) throws Exception{
        // TODO code application logic here
        List<String> lines=FileUtils.readLines(new File("temp.txt"), "utf-8");
        for(String line : lines){
            line=line.split("(\\s)+")[0];
            if(line.length()>1){
                System.out.println(line);
            }
        }
    }
    
}
