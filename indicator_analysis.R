library('randomForest')
library('Boruta')
library('reprtree')

d=read.csv('indicator_analysis.csv')
set.seed(50)
d$type="L"
d[d$value>92.77,]$type="H"
d$type=factor(d$type)
d1=d[!(d$year==2023 & d$month==5),]
d1=d1[!(d1$year==2023 & d1$month==6),]
t1=d[d$year==2023 & d$month==5,]
t2=d[d$year==2023 & d$month==6,]
set.seed(100)
d1=rbind(d1, t1[sample(1:nrow(t1), 2000),])
d1=rbind(d1, t2[sample(1:nrow(t2), 2000),])
d1$type="H"
d1[d1$delta< -0.006364,]$type="L"
d1$type=factor(d1$type)
#randomForest(type~v1+v2+v3+v4+v5, data=d1)
v1=aggregate(v1~year+month, data=d1, FUN = mean)
v2=aggregate(v2~year+month, data=d1, FUN = mean)
v3=aggregate(v3~year+month, data=d1, FUN = mean)
v4=aggregate(v4~year+month, data=d1, FUN = mean)
v5=aggregate(v5~year+month, data=d1, FUN = mean)

d2=merge(v1, v2, by=c('year','month'))
d2=merge(d2, v3, by=c('year','month'))
d2=merge(d2, v4, by=c('year','month'))
d2=merge(d2, v5, by=c('year','month'))
d2=merge(d2, unique(d1[,c('year','month','signal','value','type')]), by=c('year', 'month'), all.x=TRUE)

rm(v1,v2,v3,v4,v5)
d2$type="H"
d2[d2$signal < 17.5,]$type="L"
d2$type=factor(d2$type)
set.seed(220)
rf=randomForest(type~v4+v5, data=d2)
reprtree:::plot.getTree(rf)