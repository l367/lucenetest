邁向資本市場新里程碑 寶奇生技登錄創櫃板 - 產業特刊	
2023-11-09	
https://www.chinatimes.com/newspapers/20231109000483-260210	
櫃台買賣中心近期公告，寶奇生技公司在安永會計師事務所輔導下，已建置ERP系統並完成內容導入，日前也通過內部控制與會計制度實地審查，取得股票代號（7650）並自10月25日起登錄至創櫃板。
董事長黃志博表示，寶奇生技面對這兩年雞蛋市場危機與禽流感疫情夾擊下，在機能蛋市場上持續創新與努力下，仍取得了豐碩的成果。除了在去年，獲得經濟部新創事業獎及成功實施數位轉型計畫肯定之外，今年還成功的完成pre-A輪2,000萬元募資，完善公司財務結構，往規劃中的下一個里程碑邁進。
總經理李金燕指出，接下半來年將以推動「ESG蛋白質聯盟」為主軸，加入魚肉蛋奶豆等產品，透過供應商管理系統、經銷商與團媽管理系統、訂閱制管理系統來擴大商品提供服務範圍，進一步提升營收。同時，為了建構更完整的供應鏈與降低成本，將積極採取實質行動，在新竹縣竹北市麻園地區簽訂十年租約承租畜牧場，將打造最新的ESG永續綠能與智能示範場。
李金燕說，為了讓公司生產領域更加環保，也將與農會和地方發展協會合作，結合在地元素成為提供科普教育和企業參訪的重要場所。她說，寶奇生技將以這個示範場為基地，為客戶提供更多關於ESG永續綠色畜牧供應鏈的解決方案，從而協助推進社會的可持續發展。
黃志博則強調，這將成為各界對於ESG議題探討的重要平台，透過知識分享與技術展示，共同推動綠色科技的發展。此外，寶奇生技計劃投入地方創生，不論是與當地的客家菜美食或南寮漁港景區海鮮美食合作，皆可形成一日遊路線，為遊客帶來更豐富的體驗。這樣的整合模式，不僅將為當地經濟發展帶來積極影響，也鼓勵民眾更多地關注健康飲食和永續綠能生活。
寶奇生技目前除有上千個藥局、超市、有機店和團購據點外，訂閱制的雞蛋管家客戶也是全國之冠。近期更投資連鎖拉麵店，借重該公司的豐富店面經驗，取得雞白湯的生產技術，最後再將下腳料透入寵物食品，讓產銷一貫化完全利用。同時，一如既往地秉持著「品質至上、不斷創新」的企業精神，持續為社會與客戶創造更多價值，共同見證機能蛋公司在ESG永續綠能與智能示範場及資本市場的新里程碑。