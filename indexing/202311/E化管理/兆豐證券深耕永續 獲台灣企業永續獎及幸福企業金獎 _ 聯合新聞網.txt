兆豐證券深耕永續 獲台灣企業永續獎及幸福企業金獎 | 聯合新聞網	
2023-11-20	
https://udn.com/news/story/7253/7586656	
兆豐證券致力打造性別平等、友善職場有成，近日榮獲第十六屆TCSA台灣企業永續獎之永續單項績效「社會共融類-性別平等領袖獎」，及1111人力銀行「2023金融管顧類幸福企業金獎」雙獎肯定。這顯現兆豐證券珍視員工、關注性別平權以及建立友善職場，致力邁向企業永續經營的努力。兆豐證券遵循兆豐金控集團重視性別平等與友善職場理念，從內部法規規範金融商品廣告不得有性別歧視偏見及差別對待，並落實監控、評估金融商品服務是否符合客戶需求。在人才結構中，女性董監事比例過去三年(2020年至2022年)維持平均60%，女性員工比例平均66%，女性主管比例平均近50%且逐年上升。幸福企業亦是落實ESG的重要指標。由1111人力銀行所主辦的2023年幸福企業票選活動，兆豐證券第三度蟬聯幸福企業金獎，延續自2021年起的3E計畫：「提升員工敬業度」、「數位轉型E化」及「落實ESG」，希望讓員工不只專業更要樂業。在提升員工敬業度方面，持續強化員工的教育訓練，辦理新人培訓及為在職員工舉辦多元性專業職能課程。為吸納人才，與各大專院校合作「實習生計畫」，不僅有紮實的實務課程見習並提供多項補助，且畢業後有機會成為正式員工。兆豐證券同時啟動數位轉型Ｅ化專案，從升級資安防護到數位行銷應用等多元面向，提升內部營運效率同時優化客戶體驗。此外，兆豐證券也積極推動ESG，於國中小學內捐贈設置「金融理財愛的書庫」，亦與數家大專院校合作舉辦金融講座，實現普惠金融。另一方面為照顧員工並吸引優秀人才，亦規劃完善的員工福利，包括優於法規的健檢制度、員工協助方案（EAP），及多元的補助項目。兆豐證券深知人才是企業經營的基石，未來將持續優化職場環境以提升員工滿意度，創造員工兼具敬業、樂業、專業的工作場域，成為企業永續發展的基礎。▪ 「存款All in」買房值不值？ 網嘆：再猶豫連機會都沒有
 ▪ 台灣人超愛買房？10年來房貸金額大增4兆元
 ▪ 多國房市連環爆台灣卻很穩 銀行老總認為台灣做對兩件事
 ▪ 交易量增房價將漲？李同榮：五大理由空頭才要開始
 ▪ 月租近21萬、1坪破4千 東區酒店式公寓租金創高價