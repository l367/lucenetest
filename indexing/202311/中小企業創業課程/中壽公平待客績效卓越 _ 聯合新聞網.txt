中壽公平待客績效卓越 | 聯合新聞網	
2023-11-20	
https://udn.com/news/story/7239/7585058	
中國人壽長期秉持「公平同理，待客如己」的企業理念，落實金融消費者保護，並積極響應政府政策，實踐普惠金融，今年再寫新頁，連續五度榮獲主管機關評選成為「公平待客原則評核」排行前25%績優業者，為保險業界中極少數自公平待客評鑑開辦以來，年年蟬聯該獎項的業者；同時亦獲微型保險競賽業務績優獎、身心障礙關懷獎及高齡化保險競賽表現優良獎等大獎肯定，其中微型保險業務更是連續九年獲獎。中國人壽總經理黃淑芬表示，中壽高度重視公司永續經營能力及責任，從誠信治理與普惠關懷二大層面，落實公平待客理念，秉持「愛與關懷」的核心精神，長期推動微型保險、小額終老保險，優化服務流程，力求保障金融弱勢族群的財務安全及自身權益，帶來有溫度的保險體驗。中壽致力於維護保戶權益與福祉，同時與新住民家庭成長協會、台灣失智症協會、聽障人協會、多扶無障礙生活與交通發展協會等機構合作，導入多國語言真人口譯、手語翻譯、九國語言「保單重要權益提醒」等金融友善服務，首家原創「智齡小學堂」線上線下推動金融防詐，進一步整合公司資源推動作業面各環節防詐，以「高齡金融守護者」自許，為業界首家推出「高齡客戶友善服務指南」，從上而下推動全員金融友善專業。在弱勢族群的關懷上，中壽自2014年起大力推動微型保險，2022年微型保險累計保費較前一年度成長91%，今年陸續與宜蘭縣、雲林縣、花蓮縣、台東縣、台南市以及高雄市合作，已提供全國約10萬名符合條件的弱勢民眾享有微型保險的基本保障。此外，積極推動小額終老保險，發揮產業責任與人本精神。中壽在「以客戶為中心」的理念下，將持續落實公平待客的產業價值與社會共榮精神，連結青銀世代、社會各族群及各產業領域，全面實踐對保戶的守護和承諾，致力於協助每一位客戶實現幸福人生。▪ 「存款All in」買房值不值？ 網嘆：再猶豫連機會都沒有
 ▪ 台灣人超愛買房？10年來房貸金額大增4兆元
 ▪ 多國房市連環爆台灣卻很穩 銀行老總認為台灣做對兩件事
 ▪ 交易量增房價將漲？李同榮：五大理由空頭才要開始
 ▪ 月租近21萬、1坪破4千 東區酒店式公寓租金創高價