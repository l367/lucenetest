提升微企競爭力 勞發署桃竹苗分署推創業輔導顧問  - 生活	
2023-11-30	
https://www.chinatimes.com/realtimenews/20231130003390-260405	
勞動部勞動力發展署桃竹苗分署為協助想創業的民眾及提升微型企業經營能力，持續辦理免費創業研習課程及創業諮詢輔導等服務措施，2021年起加值提供「貸後培力與服務」，由30名專業創業輔導顧問提供財務、行銷、經營管理等面向的訪視診斷，透過分署支持輔導服務，陪伴創業店家成長精進，持續強化創業店家經營能量，維持穩定經營並加速企業成長。

桃竹苗分署長林淑媛表示，微型創業鳳凰貸款者多屬微型企業，以進入門檻低的批發零售業及住宿餐飲業為主，今年微型創業鳳凰貸款計畫45歲以上的中高齡者參與課程分別為525人次、輔導90人次及協助申請貸款68人次，提供適時的輔助可延長事業生命週期並維持穩定經營。

其中中高齡創業者，所需克服的挑戰，有如資金缺乏、資源不足、缺乏行銷管道等，透過勞動部推動多年的微型創業鳳凰貸款能減輕創業資金壓力，桃竹苗分署特別加值推出貸後培力與服務，陪伴創業過程中，提供階段性穩扎穩打創業研習課程、解決疑難雜症的專業顧問諮詢，逐步解決創業者的困境，縮短創業摸索期。

位於龍潭的瓦克食憩坊的趙蕙敏盼開發周邊居民市場、提升知名度，在輔導顧問協助下調整菜單、設計新產品及促銷方案等，讓營收成長近2成；另位在中壢區的北茶屋飲料店老闆鄭敏爵，因手搖飲料在台普及率過高，而導致在市場上並無特點，經過輔導顧問的協助下選定標記性主力商品、找出與市場有強烈差異的新品並重新設計菜單，提升營運整體競爭力。

林淑媛說明，桃竹苗分署自2017年4月推廣微型創業鳳凰貸款至今，在創業輔導與課程服務超過16000人次，6年來累計協助627人取得貸款資金，鼓勵有創業需求的民眾申請，並提供免費創業課程及顧問諮詢輔導，協助民眾風險評估、認知創業方向及強化事業經營能力，藉此提高創業成功率，且額度最高200萬元，不但低利率、免保證人、免擔保品，勞動部全額補助前2年的利息，歡迎上微型創業鳳凰網站(https：／／beboss.wda.gov.tw)或洽詢免費創業諮詢專線0800-092-957。

林淑媛提到，勞動部為解決我國人口高齡化，自2020年12月起推動中高齡者及高齡者就業促進法，為提升中高齡者勞動參與、促進高齡者就業，分別對於雇主及中高齡勞工提供不同措施，並支持中高齡者及高齡者重返職場，運用專業長才，透過跨世代共事促進職場中世代合作與交流，鼓勵雇主釋出就業機會，積極進用狀世代投入就業市場，相關資訊請上台灣就業通首頁(網址：https：／／www.taiwanjobs.gov.tw)查詢。

此外，勞動部還新建置「45＋就業資源網」(網址：https：／／45plus.wda.gov.tw／)，整合所有中高齡者及高齡者就業促進相關資源，讓民眾及雇主可查詢相關資訊及隨時掌握職缺最新訊息。