考試院會通過 113年起高考三級新增司法行政類科 | 聯合新聞網	
2023-11-09	
https://udn.com/news/story/6939/7562198	
應司法院辦理各項司法新制及改革的規劃、執行，考試院會今天通過修正高普考試規則，113年起高考三級新增司法行政類科；另外，也在衛福部建議下，社會行政類科減列2科專業科目。考試院透過新聞稿表示，院會今天通過公務人員高等考試三級考試暨普通考試規則第12條、第2條附表一及第4條附表三修正案。考試院說，為應司法院辦理各項司法新制及改革的規劃、執行與司法行政法令研修，以及各項對內整合、對外協調業務的用人需求，從民國113年起，高考三級將新增司法行政職系司法行政類科。考試院指出，同時參酌司法院建議及高普考試減列科目原則，專業科目列考「民法」、「刑法」、「民事訴訟法與刑事訴訟法」、「行政法與行政救濟法」4科，以進用司法行政相關專業人員。考試院表示，為使考選策略持續朝專業知能核心化的方向精進，113年起將實施高普考試規則修正案，高考三級應試專業科目以減列2科為原則，但部分類科因尊重用人機關研議意見，未予減列。考試院指出，衛生福利部建議，將社會行政類科列考的「社會福利服務」及「社會政策與社會立法」2科目，合併修正為「社會福利政策與法規」1科目，並刪除「社會研究法」科目。考試院表示，因此再修正高普考試規則，從113年起社會行政類科專業科目減少2科，列考「行政法」、「社會福利政策與法規」、「社會學」及「社會工作」等4科。考試院說，這次高考三級新增司法行政類科，並減列社會行政類科考試科目，主要是考量各專業領域的實務運作需求，同時確保高考三級的應試類科及科目能夠與時俱進，以提升公務人員的專業素質與應對能力。▪ 頂大新課綱首屆學生能力「雙峰化」 這類考招管道入學生高分群較少
 ▪ 台大碩博生淪廉價勞工？低工資、高工時 逾3成需兼2份以上工作
 ▪ 北部私中畢旅「日本5天4夜」要4萬7千元 校方：嚴選旅行社
 ▪ 比數學難？國一藝術課本8圖猜卡通人物 正解曝光網直呼：太難了吧
 ▪ 台大提供休學申請書特別標注「新生自取」 內行人曝3關鍵原因
 ▪ 【專題】數位原生世代大揭密 青少年邊玩邊學的網路世界