KPMG：年關近 盡早準備一次性移轉訂價申請 | 聯合新聞網	
2023-11-06	
https://udn.com/news/story/7243/7555368	
為管理集團全球稅務風險，跨國企業於執行移轉訂價政策時，實務上常有需進行一次性移轉訂價調整之情形。為提供企業明確規範，財政部於2019年發布台財稅第10804629000號令釋，允許企業在符合要件的情況下，得進行一次性移轉訂價調整。針對進口貨物，關務署亦發布「海關實施會計年度一次性移轉訂價核定完稅價格作業要點」，協助企業就進口貨物價格進行相應調整。KPMG安侯建業稅務投資部會計師丁傳倫提醒，根據海關作業要點規定，針對年底欲辦理一次性移轉訂價調整之貨物，進口時該報單即應規定註記辦理一次性移轉價調整作業，否則未註記之報單則無法於年度結束後辦理價格調整。此外，因一次性移轉訂價之申請，需於年度結束後一個月內提交予海關，因此，企業應及早檢視本年度公司損益及移轉訂價執行狀況，及早就準備本年度貨物完稅價格之申請資料，以使申請流程更為順暢。此外，為增進完稅價格審核效率與徵納和諧，海關關務署於9月舉行「海關實施會計年度一次性移轉訂價核定完稅價格作業」說明會。KPMG安侯建業稅務投資部經理陳萱分享，根據關務署過去三年審理一次性移轉訂價申請案之經驗，注意到部分申請人未於事前針對受控交易參與人應事先就其交易條件及所有影響訂價因素達成協議，導致不符合申請要件，而未能申請之情況。因此，本年度有意辦理調整之廠商，應盡快檢視受控交易之合約內容是否符合解釋令的要求。另陳萱表示，根據協助申請的經驗發現，公司在開始導入海關作業辦法之進口流程時，往往需較長之溝通時間，導致年度中僅有少數報單有註記辦理一次性移轉價調整作業，使全年度利潤集中於少數報單調整，導致貨物之價格調整幅度過大，而被海關要求提示更多資料等情況。因此，建議企業應評估貨物進口及營運狀況，針對「全年度利潤」、「全年度進口報單」及「全年度同樣貨物完稅價格」作綜合考量，對同樣貨物作趨勢一致及相同調整，且應避免將全年度利潤集中於少數報單調整，避免調整後金額異常，而面臨於海關審核期間，需耗費較多時間與海關溝通及說明價格調整情況，甚至調整後金額遭否準，而最終致所得稅上無法進行調整，引發雙重課稅風險。▪ 「存款All in」買房值不值？ 網嘆：再猶豫連機會都沒有
 ▪ 台灣人超愛買房？10年來房貸金額大增4兆元
 ▪ 多國房市連環爆台灣卻很穩 銀行老總認為台灣做對兩件事
 ▪ 交易量增房價將漲？李同榮：五大理由空頭才要開始
 ▪ 月租近21萬、1坪破4千 東區酒店式公寓租金創高價