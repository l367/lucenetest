將出席侯友宜便當會 賴士葆：提醒能源政策要有亮點 | 聯合新聞網	
2023-08-02	
https://udn.com/news/story/6656/7342671	
國民黨總統參選人侯友宜明天將與北市立委與立委參選人便當餐敘，立委賴士葆表示，明天將會親自出席，主要就是為他加油打氣祝福其民調衝高，他提到，侯辦將在9日提出能源政策，這是民眾很關心議題，會建議政策一定要有亮點。賴士葆說，侯辦現在有金溥聰加入，選戰可以不用太擔心，每個競辦都有自己的節奏與打法，他相信侯辦也都有自己策略，外人不用太多指手畫腳，有選舉過的人就知道，自己對於選戰策略不會多說什麼。賴士葆表示，明天便當會自己會親自出席，至於要和侯友宜說什麼，他表示，現在能源政策和兩岸政策是民眾最關心，侯辦打算在9日提出能源政策，建議一定要有亮點，讓民眾有感，其他就是幫母雞加油打氣，扮演小雞的角色。