紐西蘭留台青年齊聚奧克蘭 分享寶島經驗盼深化交流 | 聯合新聞網	
2023-08-25	
https://udn.com/news/story/6885/7394842	
駐奧克蘭辦事處首度於疫情後舉辦「留台校友會」活動，昨日邀請曾赴台灣研習華語、交換學生、企業實習、及從事文化、原民、體育等各式交流計畫的紐西蘭青年們齊聚一堂，數名即將於年底赴台交流的青年也共同參與。駐奧克蘭辦事處今天發布新聞稿指出，該處24日在奧克蘭大學活動廳舉辦「留台校友會」活動，與會者分享在台灣的難忘經驗，高度推崇台灣優質學習環境、企業文化、山水風光及美好人情，眾口一致說，訪台之行讓人收穫滿滿，值得跟更多紐西蘭朋友介紹推薦。新聞稿指出，包括奧克蘭大學、奧克蘭理工大學、「亞洲紐西蘭基金會」、Tupu Toa毛利信託組織等機構都派員參加，活動中彼此交流聯誼，並再討論訪台交流的最新資訊，持續加深、加廣與台灣的連結。駐奧克蘭辦事處處長陳詠韶在活動上介紹紐西蘭為新南向國家的目標國之一，台灣與紐西蘭兩國人本交流十分密切。她說：「台灣卓具華語學習優勢，台灣的高科技與資訊通信電子產業聞名全球，中小企業靈活、創新。」她表示，今年已有奧克蘭大學與懷卡托大學的學生前往清華大學、成功大學及清華大學研修華語，「亞洲紐西蘭基金會」、Tupu Toa毛利信託組織也都有選送青年到台灣的企業實習。另外，台紐藝術家相互駐村與佈展，毛利尋根計畫及參加台灣原民運動會等活動，影視合作、體育交流等各式活動也都持續進行。她稱許留台校友都是台紐友誼的最佳橋樑，鼓勵大家持續對接台紐更多良好的合作。活動隨後由奧克蘭大學、「亞洲紐西蘭基金會」、Tupu Toa毛利信託組織等留台校友，分別就在台研習華語、企業實習、及文化交流等三大領域分享經驗見聞。奧大學生馬修（Charlie Matthews）對成功大學華語專班的豐富文化課程、學伴友誼、台南美食念念不忘，戴姆勒（Ruby Dalmer）參加清華大學優華語計畫，對台灣自由開明學風、人民熱情好善、山海風光迷人多所推崇。曾在「台美合作推動中心」實習的馬歇爾（TimMarshall）則稱許台灣作為亞太商務據點的優勢，Tupu Toa計畫經理艾歐諾（Sofara Aiono）則分享在台企業實習期間，同時體驗毛利與台灣原住民族的親近性。「亞洲紐西蘭基金會」藝術主任古柏（CraigCooper）則介紹台灣藝文軟硬體傲人，原民藝術創作細緻，期許雙邊交流更加密切。辦事處同仁並接著介紹各式獎學金與交流計畫，也介紹青年度假打工與就業金卡等資訊，許多校友紛紛詢問細節，有意未來再度訪台灣深化互動。「留台校友會」活動現場提供備有道地的台式珍珠奶茶、鹹酥雞、麻糬、綠豆糕等茶點，讓許多校友感到驚喜不斷，更加懷念台灣的美好經歷。辦事處稱，這是因疫情時隔三年後再度舉辦校友活動，校友們報名參與十分踴躍，有些校友即便已經畢業了也特別前來參與，實體互動交流更添溫暖熱度，並相約密切聯繫，擴大台灣之友的影響力。駐奧克蘭辦事處處長陳詠韶（中）在「留台校友會」活動上表示，台灣與紐西蘭兩國人民交流十分密切。圖／中央社（駐奧克蘭辦事處提供）▪ 青少年的網路世界：「數位原生世代」問卷填答 快來分享你的看法！
 ▪ 整理包／112年升大學分科測驗 7科完整試題與解答
 ▪ 分科測驗／紅利不再！補教估台大醫284級分 缺額校系可這樣選填
 ▪ 分科化學／考生嘆不易！非選題計算多 高中師指「這1題」有爭議
 ▪ 分科數甲／計算量大、考斜橢圓「好生疏」 考生：很多人寫不完
 ▪ 分科生物／COVID-19、猴痘時事入題 考生提早交卷：簡單
 ▪ 分科歷史／中國史比重多、考白色恐怖 考生：有刷考古題能拿分
 ▪ 分科公民／NCC換照、ME TOO都入題 考生作答想好久直呼「好難」
 ▪ 白飯之亂 北科大挨轟「教育失敗」遭洗版剩1.5顆星
 ▪ 專題／學習歷程，別鬧了！首屆108課綱生心路大公開