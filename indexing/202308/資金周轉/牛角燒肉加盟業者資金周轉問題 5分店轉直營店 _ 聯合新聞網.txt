牛角燒肉加盟業者資金周轉問題 5分店轉直營店 | 聯合新聞網	
2023-08-01	
https://udn.com/news/story/7270/7340075	
日本品牌燒肉店「牛角」的加盟店捷利國際餐飲股份有限公司發生資金周轉問題，導致7分店日前暫停營業。牛角今天表示，其中5家分店將轉為直營店，從8月起陸續恢復營運，舊有員工有意願也都會轉為直營店員工。日本品牌燒肉店「牛角」在官網表示，加盟店捷利國際餐飲股份有限公司發生資金周轉問題，導致食材採購等出現困難而無法營業，7間分店7月25日起暫停營業，其他分店正常營業。暫停營業分店包括高雄漢神巨蛋店、高雄同盟店、高雄義大店、屏東太平洋店、台中豐原太平洋店、嘉義耐斯店及台南永康愛買店。牛角今天發布新聞稿表示，原捷利國際餐飲股份有限公司旗下所加盟的「牛角日本燒肉專門店」現回歸台灣直營總部東京牛角股份有限公司，5家分店，包含台中豐原太平洋店、嘉義耐斯店、台南永康愛買店、高雄義大店、屏東太平洋店8月起陸續恢復正常營運。員工部分，牛角指出，預計恢復營業的分店中，有意願受雇於直營店繼續工作者，都會聘舊有員工，且為保障其生活，牛角總部東京牛角股份有限公司將在可判定範圍內提供部分金額作為生活補助，並提供8月 1日起雇用的就業諮詢。