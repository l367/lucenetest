卡努颱風來襲 北北基停班停課 3日股匯休市 - 財經	
2023-08-02	
https://www.chinatimes.com/realtimenews/20230802005876-260410	
台股在颱風天是否休市，集中交易市場休市原則以台北市為準。當台北市宣布全體公教機關全天停止上班、或是上午停止上班時，集中交易市場全日休市，全體證券商當日停止營業，且當日應屆交割款券順延辦理。央行表示，因國際金融市場持續營業，相關主管及交易員仍需到行上班，除從事國際金融交易，也主動與銀行聯繫，緊急提供外幣資金，以維持各銀行外幣資金的正常運作。央行指出，在票據交換方面，為顧及支票存款戶資金調度受到影響，央行督導台灣票據交換所對於支票存款戶所發生的存款不足退票，以及因本次颱風遭受災害，致影響票款清償能力者，依據相關規定予以從寬處理。在跨行清算作業方面，央行已請財金公司督促各銀行撥存充足資金於跨行清算專戶。