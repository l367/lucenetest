鴻海被指逼退前技術長 新北勞工局：有調解卻調整未成
2023-02-07
https://udn.com/news/story/7323/6956184
鴻海集團逼退前技術長魏國章，遭求償6200萬元，新北地方法院將在2月下旬開庭審理。對於鴻海集團與魏國章的勞資糾紛問題，新北市勞工局證實，在去年10月有接獲勞資調解申請，但雙方各持己見，因此調解不成立。 勞工局表示，在去年10月下旬接獲調解申請，11月中旬召開調解會議，惟雙方對於契約關係是否屬勞動基準法上之「僱傭關係」，或依公司法規定之「委任關係」各持己見，因此調解不成立，本案目前已進入司法訴訟程序，後續將依法院審理與判決為據。 勞工局 鴻海