2月電子期金融期齊跌
2023-02-15
https://udn.com/news/story/7255/6972835
台北股市今天下跌221.59點，收15,432.89點。2月電子期收723.15點，下跌16.9點，逆價差2.04點；2月金融期收1,556.8點，下跌13點，逆價差1.9點。 2月電子期以723.15點作收，下跌16.9點，成交736口。3月電子期以721.6點作收，下跌17.5點，成交1,149口。 電子現貨以725.19點作收，下跌14.61點；2月電子期貨與現貨相較，逆價差2.04點。 2月金融期以1,556.8點作收，下跌13點，成交625口。3月金融期以1,559點作收，下跌11點，成交783口。 金融現貨以1,558.7點作收，下跌13.8點；2月金融期貨與現貨相較，逆價差1.9點。實際收盤價以期交所公告為準。 逆價差 金融期