國銀放款中小企 民營銀行包辦前五大
2023-06-14
https://udn.com/news/story/7239/7233310?from=udn-referralnews_ch2artbottom
據金管會昨（13）日統計，今年前四月國銀對中小企業放款增加前五大，全部清一色都是民營銀行，反觀中小企業放款占比逾八成的公股行庫卻是減少、或是持平，呈現「民進公退」的此消彼漲態勢。 為何「民進公退」？銀行主管說，因行庫對中小企業放款規模大、餘額也較高，加上3月基期已墊高，4月再往上大增的空間有限，就5月來看，行庫對中小企業放款已開始加速成長。 數據顯示，今年前四月國銀對中小企業放款增加最多前五大，第一是台北富邦銀行增423億元、中國信託商銀增149億元次之、永豐銀行增130億元、國泰世華銀行增59億元、聯邦銀行增57億元。銀行局副局長童政彰說，北富銀增加逾400億元主因是4月和日盛銀合併，把日盛銀放款併入所致。 若以餘額數字來看，前五大國銀中小企業放款餘額最高的，則清一色都是行庫。據4月底統計，第一銀行中小企業放款餘額9,087億元、合庫銀行8,336億元、台灣中小企業銀行7,273億元、華南銀行6,847億元，兆豐銀行6,167億元。若與3月底相較，除台企銀和華銀各略增3億元和5億元，一銀、兆豐銀各減少33億元和31億元，合庫減少9億元。 台灣中小企業銀行 兆豐銀行