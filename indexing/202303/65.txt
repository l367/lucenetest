央行升息衝擊銀行利率 鄭文燦：這4部分由政府吸收
2023-03-24
https://udn.com/news/story/7239/7053415?from=udn-referralnews_ch2artbottom
央行昨（23）日決議升息半碼，已是連續五次升息，重貼現率來到1.875％，創下近八年新高。對此，行政院副院長鄭文燦今（24）日表示，升息的半碼將由政府吸收，包括包含學貸、勞工紓困貸款、中小企業的紓困振興貸款，以及青創貸款。對於房貸的衝擊則要審慎研議。 鄭文燦今日出席2023 TALENT, in Taiwan 台灣人才永續行動論壇「多元 x 平等 x 共融：人才管理新戰略」時被問及，針對央行昨日升息半碼的消息，政院這邊是否有相關配套因應民眾的衝擊？ 鄭文燦表示，有關升息所帶來的影響，我們已經進行了各種方案的評估。其中升息的半碼，由政府吸收的部分，包含學貸、勞工紓困貸款、中小企業的紓困振興貸款，以及青創貸款。 至於房貸的部分，鄭文燦說，財政部跟金管會會審慎研議，看有關這個貸款的部分，如何縮小衝擊。 升息 鄭文燦