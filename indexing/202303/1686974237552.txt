中國兩會審議黨政機構改革方案 學者：強黨弱政
2023-03-06
https://udn.com/news/story/123410/7012611?from=udn-relatednews_ch2
中國今年「兩會」焦點之一是審議「黨和國家機構改革方案」。有學者表示，在中共20大報告中「安全」一詞出現了91次，維護國家安全已是重中之重，而改革特色是「強黨弱政」。 國策研究院及國立中山大學中國與亞太區域研究所今天主辦「中共兩會：黨政軍經、美中與兩岸」座談會，淡江大學大陸研究所教授張五岳在會中作上述表示。 張五岳表示，2022年中共20大報告中，「安全」一詞出現了91次，即在高舉安全議題，「怎麼可能採取相對措施，人事部署上不落實」？因此「安全」將會是「黨和國家機構改革方案」的重中之重議題。 他表示，目前已有國家安全委員會，國安委是黨內最高的權力機關，國安委主任是中共總書記習近平，副主任是人大委員長跟國務院總理，國安委辦公室主任就是中共中央辦公廳主任、中央書記處書記，這三個是一體的。 張五岳表示，除了安全議題外，應付金融風險也成為中共的重點工作，其餘包括科技領域，這幾項改革的特色是強黨落政，加強黨對一切的領導。 他又表示，「兩會」期間需要留意中共高層的講話，包括習近平及王滬寧會不會下團組發表涉台講話。習近平會在今年「兩會」閉幕連任國家主席，屆時一定會發表有關涉台的講話。 他表示，2013年3月17日中國12屆人大一次會議閉幕，當時習近平涉台講話有50字，2018年3月10日中國13屆人大第一次會議閉幕，習近平發表講話時，涉台內容有250字，相信今年閉幕的講話也一定提到台灣議題，中共對台灣的立場將更加明顯。 中共 習近平 國安