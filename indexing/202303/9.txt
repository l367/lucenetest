陸三大航空去年虧損4800億元 吐回6至9年獲利
2023-03-31
https://udn.com/news/story/7333/7069261
大陸三大航空公司2022年獲利已全數出爐，據統計，三家航空公司總虧損逾千億人民幣，虧掉了疫情前6至9年的總獲利不等。大陸官方則已表態力爭2023年「盈虧平衡」。 大陸三大航空分別是國航、東航與南航。據澎湃新聞報導，三大航去年生產經營遭受嚴峻挑戰，營收共計人民幣1,860.68億元（約合台幣8,240億元），同比下降約23.52%；歸母淨虧損合計1086.87億元（約合台幣4,813.2億元），同比擴大超過1.65倍。 其中，三家航空公司虧損都超過人民幣300億元，國航虧損最高達386.19億元（約合台幣1,710億元），南航最少。報導指，此次國航虧掉了疫情前6年（即2014年至2019年）的歸母利潤累計總額，東航和南航則虧掉了疫情前9年（即2011年至2019年）的歸母利潤累計總額。 此外，國航的年報提及，2022年大陸民航業各經營主體全年虧損高達2,160億元（約合台幣9,565.7億元），超過前兩年虧損額的加總。 對於2023年趨勢，國航指出，航空市場預計競爭壓力將有所緩解，預計內部市場需求將持續復甦向好；南航則提到，將密切關注政策變化，全力爭取保障資源和境外時刻，有序恢復國際航班；東航則提及，持續加強航網建設，提升京津冀、長三角、粵港澳大灣區、成渝經濟區的市場競爭力。 此前在2023年1月，大陸官方召開的「全國民航工作會議」，已提到要把握好行業恢復發展的節奏，優化調整防控策略，積極開拓航空市場，提高協同運作水平。「2023年，力爭完成運輸總週轉量976億噸公里，旅客運輸量4.6億人次，貨郵運輸量617萬噸，總體恢復至疫情前75%左右水準，力爭實現盈虧平衡」。 大陸在3月26日至10月28日，已開始執行夏秋季航班計畫，大陸民航局3月24日指出，「今年夏秋航季，共有169家國內外航空公司計劃每週安排客貨運航班11萬7,222班，國際客運航班計劃量較2022年夏秋航季有明顯增長」。具體而言，港澳台航線增長30.83%、大陸內部航線微幅下降0.39%，國際航線則在2022年多條航班遭「熔斷」的基礎下增長更多，光是2022年10月30日開始的冬春航季航班，就較2021年增長105.9%。 國際航班 疫情 台幣