政院金援中小企業 8,000億元銀彈上膛
2023-03-28
https://udn.com/news/story/7238/7060535
挺中小企業，行政院昨（27）日公布「疫後振興」及「轉型發展」兩項專案貸款計畫，合計將備妥8,000億元資金，由政府補貼貸款利率1.595%，讓中小企借新還舊，或者進行低碳化、智慧化轉型，預估兩項專案性貸款可讓20萬家企業受惠。 經濟部工業局長連錦漳表示，疫後振興專案貸款是因應這波景氣下滑，讓中小企業及微型企業能借新還舊，喘一口氣。 針對中小企業，借新還舊最高貸款額度為3,500萬元，最高貸款利率為2.095%，政府補貼利息1.595%，為期一年。經濟部中小企業處副處長陳秘順指出，現行中小企業定義是資本額1億元以下、員工人數200人以下，只要擇一符合，就可適用疫後振興專案貸款。 至於五人以下微型企業、及僅有稅籍登記小店家也能享有專案貸款，最高貸款額度為400萬元；經濟部亦已與銀行團協商好，若貸100萬之內，貸款利率為1.595%，全額由政府補貼利息；而超出100萬元部分，貸款利率最高2.095%，業者也只要負擔0.5%利息。政府補貼利息二年。 利息 行政院