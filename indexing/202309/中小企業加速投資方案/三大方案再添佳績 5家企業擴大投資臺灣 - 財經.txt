三大方案再添佳績 5家企業擴大投資臺灣 - 財經	
2023-09-29	
https://www.chinatimes.com/realtimenews/20230928005809-260410	
由於全球環保浪潮的興起，各國對於空污防治規定趨嚴，身為國內車用觸媒還原劑AdBlue®（柴油車用尿素溶液）主要供應商的諾瓦材料，積極投入製造環保產品，透過「智慧型儲槽加注物聯網系統（i-depot）」與獲得專利認證的「移動工廠（i-Blending system）」生產方式，達到即時庫存管理與提高生產效率。諾瓦材料以臺灣為全球營運中心，將投資逾6億元於桃園觀音區興建智慧化新廠，擴大製造觸媒還原劑與拓展廢棄物再製燃料事業；新廠將以熱泵設備取代傳統鍋爐，採用一級能效產品與建置能源管理系統等，期望為環保盡一份心力。預期投資完成後，可增加3個本國就業機會。
振鋒企業為工業起重索具配件與個人安全防護裝備製造商，以自有品牌「YOKE」行銷全球45個國家，其個人高空防墜安全設備的全球市占率達6～7成。為因應營收成長布局與客戶對於產品自製率要求提升，振鋒企業啟動三區擴廠計畫，包含於彰化全興廠增設鍛造產線、擴增臺中加工廠產能及興建臺中精科廠，設置智能化產線，以提升生產效率與強化產品競爭力，預計投資金額約75億元，可創造50名本國就業機會。振鋒企業更於111年獲選臺灣100大永續典範企業，透過導入環境管理系統、在地採購與落實碳盤查，期望實踐更低碳、永續的生產過程。
隨著萬物聯網、人工智能的時代來臨，高速運算衍生的散熱市場商機不容小覷。展緻企業以純熟的沖壓技術起家，延伸投入高階散熱模組、工業電腦機殼與無人機加工部件等市場，也因掌握精密工程技術與代工成本優勢，獲得HP、Dell、Cisco、Microsoft等國際大廠授權為衛星工廠供應商。展緻企業為了持續擴大CPU與GPU散熱器產能，規劃招聘152名本國員工，投資逾5,000萬於新北市樹林廠區增設智能化產線，降低人力使用與提升產品良率，並將設置太陽能集電板，輔助夜間照明，達到節能減碳的效益。
雅棋德科技擅長為各類材質產品進行外觀塗裝，並為全球知名自行車品牌如GIANT、Cinelli、SRAM、DECATHLON等公司，提供車架、避震前叉等產品的表面彩繪塗裝。看好休閒運動產業持續蓬勃發展，故於苗栗縣投資興建一、二廠，建置金屬表面前處理、粉體與液體塗裝產線，導入較為環保的三價鉻製程，使用智慧機器手臂、MES系統，優化產品產能與品質，提供客戶更完整的塗裝一站式服務，並為公司未來的發展打下良好基礎。本案投資完成後，可增加20名本國就業機會。
明治海洋國際主要從事魷魚、秋刀魚等水產品加工製造，由於所屬的順滿集團經營遠洋漁撈事業，對於冷凍倉儲的需求增長，決定於屏東縣南州鄉增聘80名本國員工，投資11.2億元興建水產加工廠與冷鏈倉儲物流中心；加工廠將以自動化分切清洗、尺寸分選與包裝設備取代傳統人工，大幅縮短加工製程與提高品質，並將建置冷鏈倉儲系統與食品溯源履歷，讓消費者更加瞭解漁產從捕撈、保鮮與加工保存的歷程。此外，新廠將設置太陽能集電設施與廢汙水處理系統，期望朝向綠色企業發展。