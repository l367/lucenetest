嘉義市府把關食安  稽查24通路無巴西蛋 要求廠商標示來源 - 生活	
2023-09-15	
https://www.chinatimes.com/realtimenews/20230915004447-260405	
進口蛋爆發食安疑慮，民心惶惶，嘉義市政府今天公布，衛生局目前稽查3家全聯福利中心，業者表示，收到總公司通知後，均已全數下架回收，未陳列販售巴西進口蛋；也稽查確認家樂福、大潤發未進口巴西蛋品販售。

嘉義市衛生局表示，目前已啟動「市售雞蛋稽查專案」，查核產品標示、進貨證明，包括查核是否標明原產地（國）、有效日期、標示是否有誇張不實或易生誤解之情形。

大賣場、連鎖超商、傳統雜貨店、早餐店、烘焙業、餐飲業等都是稽查重點，目前已查核銷售通路24家，都符合規定，未發現有販售或使用有疑慮蛋品。

此次進口蛋因尚未取得完整進口資料，除一般超市、賣場、雜貨店等通路外，黃敏惠市長責成市府團隊立即掌握雞蛋產銷市況，並嚴格把關學童午餐的蛋源蛋品。

今天下午消保官黃崇傑率隊與市府建設處執行聯合稽查，到共和市場訪查雞蛋的供應情形及效期。

市府建設處表示，本周傳統市場蛋價1斤約50至53元，常溫白蛋蛋價1斤50至53元，全聯福利中心洗選白蛋蛋價63元（10顆／1盒），傳統蛋專賣店及福利中心紅殼蛋蛋價1斤70至76元，均無販售進口蛋。

市府教育處表示，嘉義市國中小午餐合約長期以來均要求採用國產洗選蛋，請家長放心。

市府表示，傳統蛋專賣店及福利中心都有提供QR code追朔產地及效期，目前傳統蛋專賣店、福利中心皆販售國產雞蛋。

黃崇傑說，消費者購買蛋類前掃描QR code追朔產地及效期，如有產地不明、已過效期等疑慮，可打消費者服務專線1950或向嘉義市政府消保官投訴。