苗栗縣發放老人三節禮金 大湖鄉公所公所貼心提醒這件事 - 寶島	
2023-09-19	
https://www.chinatimes.com/realtimenews/20230919003053-260421	
苗栗縣長鍾東錦上任後全力推動發放老人三節禮金每年3千元，將從明年元旦起開始施行，大湖鄉公所配合這項政策，從19日起開始發送老人年金通知單及申請書，從從9月25日至10月20日止由各村辦公處受理，大湖鄉長黃惠琴並提醒長輩們注意防範詐騙集團藉此行騙，千萬不要讓陌生人介入協助。

苗栗縣政府發放老人三節禮金，每人每年共3千元，包括春節、端午節及重陽節等每一節為1千元，凡是設籍苗栗縣的65歲以上長輩都可領取，由於老人三節禮金將從明年開始發送，大湖鄉公所規畫從19日開始，由鄰長協助發放通知單及申請書，請有資格領取的長輩們填妥後，送交各村辦公室或是鄉公所社福課辦理登記。

大湖鄉長黃惠琴指出，老人三節禮金發放方式，統一以「匯款入帳」，接受申請登記時間從9月25日至10月20日止，長輩們可以送至各村辦公處，假日只有10月14日鄉公所所福課理。

黃惠琴並請長輩依通知單排定時間，攜帶身分證、印章、存摺（農會優先）、申請書至各村辦公處申請敬老禮金。黃惠琴提醒長輩交由家中年輕人或是村長幫忙，或是來電洽詢社福課，千萬不要問陌生人，更不要找陌生人幫忙，以免詐騙集團藉此行騙。