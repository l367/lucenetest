印尼擬修法禁社群媒體商品交易 TikTok反對 - 國際	
2023-09-13	
https://www.chinatimes.com/realtimenews/20230913005443-260408	
印尼政府擬禁止社群媒體上的商品交易，TikTok今天表達反對之意。雅加達當局批評，北京字節跳動旗下的TikTok等大型社群媒體擁有電商平台是壟斷行為。
TikTok電商部門迅速成長已在區域市場占一席之地，自2021年推出以來已有數以百萬計的賣家，TikTok的印尼用戶消費金額居東南亞之冠。
近週來，多名政府官員呼籲社群媒體和電商平台必須分割，稱TikTok等公司的壟斷作法不利當地的小型企業。
TikTok印尼溝通部門主管安基尼（Anggini Setiawan）告訴法新社：「印尼近200萬地方商家利用TikTok成長，透過社群商務蓬勃發展。」
「強制社群媒體和電子商務分割為不同的平台，不只會危害創新，也不利印尼商販和消費者。」
TikTok並呼籲印尼當局提供TikTok一個公平競爭的平台。
印尼貿易部副部長傑里（Jerry Sambuaga）昨天在國會聽證會上表示：「我們必須分拆電子商務、社群媒體和社群商務。」
他抱怨社群商務規範不足，並呼籲修改現有的商業交易法規。
他指出：「修法…將堅定明確禁止那點。」他未進一步說明他的計畫細節。
印尼現有的法規未涵蓋社群媒體交易。
美國科技巨擘Meta也利用旗下社群平台臉書（Facebook）和Instagram的電商商店。
印尼貿易部長哈山（Zulkifli Hasan）11日告訴媒體，修法後可能會要求公司替旗下的社群媒體和電子商務分別申請不同的執照。
印尼合作暨中小企業部（Ministry of Cooperatives and Small and Medium Enterprises, CSMEs）部長德登（Teten Masduki）上週告訴一個委員會，一家公司不得結合社群媒體和電子商務，並警告TikTok可能會成為「壟斷」事業。
根據TikTok數據，印尼是TikTok第2大市場，擁有1億2500名用戶。TikTok執行長周受資6月曾親赴雅加達，矢言未來幾年將在東南亞投入數十億美元。
