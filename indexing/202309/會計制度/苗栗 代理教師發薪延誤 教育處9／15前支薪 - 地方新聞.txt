苗栗 代理教師發薪延誤 教育處9／15前支薪 - 地方新聞	
2023-09-14	
https://www.chinatimes.com/newspapers/20230914000614-260107	
苗栗縣傳出部分國中小學代理教師延誤發薪，引起教師團體不滿，點名苗栗縣府1周內給薪，縣長鍾東錦13日強調「不會讓自己的員工餓肚子」，要求教育處15日前協助完成支薪。教育處指出，未來會精簡敘薪程序，為避免類似情況發生，將列入校長考績追究責任。
代理暨代課教師產業工會透過臉書表示，苗栗縣長鍾東錦2月宣布苗栗縣代理教師全年聘用，但現在開學2周薪資晚到，至今欠薪2個月，呼籲苗栗縣府1周內完成全縣代理教師給薪。
縣府教育處副處長徐建男說，苗栗國中小學目前約有200多位代理教師，經了解部分偏鄉學校因為不易聘請代理教師，加上每位教師報到時間不一，所以會等所有教師確定報到才送公文敘薪，導致少數代理教師沒領到薪水。
他表示，偏鄉學校也因為會計、人事職員不足，多由校方一般行政人員兼任，才會發生行政作業不熟悉情況，未來會加強訓練。針對敘薪問題，未來也會精簡行政程序，並加強督導機制，確保每位代理教師支薪到位。
鍾東錦指出，他慎重了解原因，是某些校區因為統一行政流程，因而造成薪資晚發的狀況，這些關乎代理教師們的權益、生計，讓他們受了委屈，他會擔起全責。他也要求教育處從制度面改善，並在15日前讓延誤支付的薪資到位。為避免類似事件重演，鍾東錦責成教育處將敘薪問題列為校長考績。
另外，現階段各縣市公務員都是每月1日發薪，苗栗縣則是5日支薪，鍾東錦認為目前財政調度沒問題，苗栗應該恢復每月1日發薪。