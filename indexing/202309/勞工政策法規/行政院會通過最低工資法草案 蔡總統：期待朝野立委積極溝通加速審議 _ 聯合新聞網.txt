行政院會通過最低工資法草案 蔡總統：期待朝野立委積極溝通加速審議 | 聯合新聞網	
2023-09-21	
https://udn.com/news/story/7238/7456294	
蔡總統今晚在臉書上表示，台灣的基本工資，在他的兩個總統任期連續八年調漲，從2016年的20,008元，到了明年，將調整為27,470元，時薪也由原本的120元調整為183元。此外，為穩定及明確地調整最低工資，希望透過立法，將最低工資的審議規範，從法規命令提升到法律層級，並且將所需參考的社會、經濟指標納入法條中，來建構完善的機制，進一步保障勞工朋友的權益。蔡總統說，今天行政院會通過的《最低工資法》草案，有幾個重要特色：一、組成「最低工資審議會」定期開會，由勞、資、政、學四方委員代表組成，藉由社會對話機制，擬定具有共識的最低工資調整方案。二、審議所需參採指標明確入法。法條將各界最具共識的「消費者物價指數年增率」列為應參採指標，並明定得參採的 10 項指標，讓審議機制能夠兼具穩定及彈性，也可因應整體經濟社會情勢通盤考量。三、建立跨領域研究小組先行評估機制。邀集學者專家及部會代表，組成跨領域研究小組，每年4月提出最低工資實施影響報告，並於審議會議30日前，完成研究報告及調整建議，作為審議參考。四、完備最低工資的議決及核定程序。法條中有明確規範，行政院不予核定時，由勞動部再行召開會議重審，消除對於最低工資調整的不確定感。她指出，過去七年多來，我們持續推動各項政策，提供勞工朋友更多的支持，對於年輕朋友，我們也提供職訓資源來協助創業。為了回應青年世代的居住需求，行政院會今天也通過了俗稱「 囤房稅 2.0」的《房屋稅條例》部分條文修正草案，將送交立法院審議。她表示，給予勞工朋友更多權益的保障，減輕青年朋友的生活負擔，是我們努力的目標。針對草案內容，行政部門會積極跟立法院各黨團溝通協調，我也期待朝野立委共同支持，加速完成審議程序。蔡總統說，今天行政院會通過的《最低工資法》草案，有幾個重要特色。取自蔡總統臉書▪ 基泰昨才漲停…今「基泰忠孝」大樓百億元成交 買家是這號人物
 ▪ 「基泰忠孝」125億元火速成交 專家點3大條件：絕佳進場時機
 ▪ 為大直事件籌錢？10天就125億元賣「基泰忠孝」大樓 基泰回應了
 ▪ 每戶總價1.4億元！土地銀行第二次標售大安區豪宅 預定11月3日開標
 ▪ 這豪宅好威！舊制壓哨前搶上車 四天就賣了21億元