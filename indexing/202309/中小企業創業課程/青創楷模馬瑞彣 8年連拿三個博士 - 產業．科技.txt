青創楷模馬瑞彣 8年連拿三個博士 - 產業．科技	
2023-09-18	
https://www.chinatimes.com/newspapers/20230918000244-260204	
台灣青創楷模暨優尼克生技董座馬瑞彣8年前在中興大學生物化學研究所就讀期間，因緣際會展開學以致用的大健康生技產品創業之路；2015年迄今創業8年，馬瑞彣利用工作閒暇，一口氣拿下醫學、生物化學、產業經濟學三個博士學位，期間並通過國際營養師、中國中西醫藥劑師等資格，2022、2023年連續2年入選馬奎斯世界名人錄，創業與學術專業兼顧令人印象深刻。
2015年在學期間，馬瑞彣即以「健康守護的專家」為宗旨，創立優尼克生技公司，利用自有專利之獨到生技技術，發展出可對抗新冠病毒的戴維爵士品牌「抗菌護膜液」產品系列；8年產學歷練，現今馬瑞彣醫學博士已是同時擁有多個博士學位、專業產業經驗及MIT年輕創業家精神於一身。
馬瑞彣表示，回想創業初期，非常幸運地遇到貴人相助，讓他在創業新鮮人階段不至於飄忽不定、無所適從，反倒學會以精準的眼光掌握市場的需求，有效率的學以致用。一路走來，經常向多位國內外學界前輩請益，不斷優化既有獨到的專利技術，致力將旗下生技與保健產品打造成改善提升民眾健康生活的優質產品，也為企業創造出逐年成長的營收與獲利，迴圈挹注企業投入更多的資源，以發展更多創新技術與產品，推動企業永續發展。
結合國內外產官學研力量，能帶給新創企業持續前進很大助力，馬瑞彣博士說，新創產業市場的變化是每天都在調整的，靈活卻不忘初心的本質，才能有機會在這紅海市場中持續的前進。因為立志要做產學兼具的企業家，因此在大學就學期間便已經立定志向，確認以產業創業之路為第一首選的道路。
馬瑞彣指出，在身分轉換的過程中，必定會碰觸到許多新技術與法規的桎梏，他很幸運地獲選第三屆行政院青年諮詢委員，在任期間結識相當多有志一同的青年創業家，大家共同前往拜會各部會，直接且有效率的了解、諮詢甚至建議法規的方針，這讓創業初期有著很大的幫助。
而在學術與研究的路上，馬瑞彣也從來不間斷，經常與世界級醫學研究單位、國內頂尖醫學大學、財團法人機構等交流合作研究，甚至自身也持續投入學術，於國內大學任教，開的課程包含藥學、食品營養、美妝、寵物美容保健開發、產業經濟及創新創業等，並將精闢的研究成果發表於國際知名文獻期刊，讓全球專家共同檢視與指教。
馬瑞彣博士強調，投入大健康產業須培養世界觀，隨時掌握國際知名期刊的最新發表的技術與文獻；他在創業初期十分積極向學，不但前往廈門大學就讀醫藥產業總裁班，8年間也陸續考取了美國醫學博士、國際營養師以及中國中西醫藥劑師，讓自身以及團隊面對全球化的大健康脈動，都能精準地掌握。
8年來，優尼克生技公司已從環境醫學的抗菌生活用品，進一步開發出護眼保健產品，未來更多新保健產品系列將一步一步開展，一直謙稱創業運氣好的馬瑞彣樂於分享創業心得，他強調年輕新創業者在創業前一定要做好下列功課，企業定位準確、找到市場需求、對社會有價值、了解競爭對手、構建商業模式、做好財務規劃，如此相信更有機會成為一位成功的創業者。