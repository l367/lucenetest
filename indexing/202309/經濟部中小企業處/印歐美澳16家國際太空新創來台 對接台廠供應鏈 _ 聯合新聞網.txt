印歐美澳16家國際太空新創來台 對接台廠供應鏈 | 聯合新聞網	
2023-09-21	
https://udn.com/news/story/7238/7456213	
經濟部中小企業處今年首度推出「2023國際太空新創來台落地培訓計畫」，吸引來自印度、歐洲等16家國際太空新創來台，促成3項MOU簽署，並協助對接近50家太空供應鏈廠商，明年擬持續推動相關計畫，協助台灣太空產業多元布局。經濟部中小企業處今天發布新聞稿表示，培訓計畫與印度、歐洲等組織合作，透過國內太空領域專家學者共同遴選出16家國際太空新創來台，包含印度7家、歐洲7家、澳洲1家以及美國1家，領域涵蓋衛星製造、衛星服務以及太空應用等。中小企業處表示，台灣ICT產業具製造優勢，可提供太空產業所需的精密、高品質零組件，加上台灣業者可提供少樣多量等彈性服務，是吸引外國新創來台的誘因；由於今年計畫執行成果不錯，規劃明年持續招募國際太空新創來台，協助對接台灣產業供應鏈。中小企業處指出，除了協助國際新創參訪國家太空中心及林口亞灣新創園，拜會台灣太空產業廠商，並參與航太國防展等，期間也協助外國新創與近50家太空供應鏈廠商鏈結，累計舉辦超過30場次商務媒合會議。值得一提的是，有3家外國新創與台灣業者簽署合作備忘錄（MOU），包含Vellon Space Private Limited與亞大基因科技股份有限公司針對太空微重力生物實驗合作，Hex20 Pty Ltd與晶泰國際科技股份有限公司聚焦太陽能電池衛星應用測試，以及Hex20 Pty Ltd與互宇向量股份有限公司在立方衛星製造合作。學研合作方面，中小企業處表示，有多項與台灣產學研的合作持續洽談中，包括太空微重力環境合作、製造電路板、特殊規格記憶體材料國產化等。中小企業處強調，目前台灣以半導體、資通訊、精密機械為基礎，98%產值聚焦地面設備，引入國際太空新創來台發展，希望在太空應用、衛星製造、衛星應用、火箭發射等領域促成國際合作，增進台灣太空產業多元布局。▪ 基泰昨才漲停…今「基泰忠孝」大樓百億元成交 買家是這號人物
 ▪ 「基泰忠孝」125億元火速成交 專家點3大條件：絕佳進場時機
 ▪ 為大直事件籌錢？10天就125億元賣「基泰忠孝」大樓 基泰回應了
 ▪ 每戶總價1.4億元！土地銀行第二次標售大安區豪宅 預定11月3日開標
 ▪ 這豪宅好威！舊制壓哨前搶上車 四天就賣了21億元