中華電信攜手臺北流行音樂中心 打造5G智慧導覽機器人 | 聯合新聞網	
2023-09-15	
https://udn.com/news/story/7240/7442436	
中華電信（2412）攜手臺北流行音樂中心，打造5G智慧導覽機器人，融合中華電信5G企業專網、AI與聯網機器人打造嶄新互動體驗。中華電信表示，憑藉著業界最大頻寬與最豐富的網路建設維運經驗，持續為國內製造、文創以及醫療等多元產業提供安全、高品質的5G專網服務。充分善用5G高速率、低延遲、大連結之特性，推動文創產業實現多元虛實整合應用服務，加速推進各產業的升級與創新，不斷為臺灣的產業發展帶來新的動能與活力。臺北流行音樂中心(簡稱北流)是臺灣北部唯一以流行音樂為主的生活園區，於經濟部中小企業處支持下，中華電信表示，結合中華電信為其打造5G的企業專網，並藉著中華電信新創5G加速器多年培育創新應用之經驗，引進聯網自主移動式機器人，同時結合新創愛吠的狗娛樂技術整合及產品開發，共創打造AI人工智慧導覽服務，以尖端純熟的AI自然生成語音技術，為蒞臨文化館的參觀民眾帶來嶄新的互動體驗。中華電信表示，透過中華電信5G專網所具備高速率、低延遲、大連結之特性，臺北流行音樂中心將自主移動式機器人與個性鮮明活潑的智慧導覽員「AI Amaze 希希」巧妙地結合在一起，成為場館內新穎吸睛的導覽助手。這項服務不僅能夠精準地根據參觀民眾所在位置，提出周詳的導覽資訊，甚至還能引導他們前往售票區、深入瞭解周遭的音樂活動、藝術展覽及美食餐廳等多元內容，以全新科技、智能應用思維打造活潑有趣的互動空間。中華電信強調。持續拓展5G應用服務，引領文化產業展現新面貌，本次與臺北流行音樂中心的智慧導覽服務合作只是其中一例，此外亦在智慧醫療、智慧交通、智慧製造、AGV/AMR、元宇宙平臺等領域取得實質成果，持續與各界先進緊密合作，拓展各垂直應用領域。▪  【話題】沒買氣？iPhone 15蘋果官網開搶 別買這款容量！全色系20分鐘秒殺
 ▪  【話題】iPhone 15能用Type-C安卓充電線嗎？蘋果官方給答案了 注意1關鍵
 ▪ 一表看iPhone 15全系列價格、顏色、規格 這款被強迫升級貴6000
 ▪ 【活動】挑戰 Apple 冷知識 iPhone 15搶先送​