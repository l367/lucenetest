虎航Q1訂位率接近全滿
2023-01-04
https://udn.com/news/story/7241/6884674
受惠於解封商機加持，台灣虎航（6757）董事長陳漢銘昨（3）日指出，第1季訂位率已近全滿，隨著各大機場的場站陸續恢復，將會陸續增班，還有許多包機業務在進行中，今年營收會逐季成長。台灣虎航是低成本航空公司，符合創新營運模式，已申請創新板上市，待審查通過後，希望最快在第1季掛牌。 台灣虎航今年第1季機票賣的非常好，目前日本開放16個航點，台灣虎航僅營運九個航點，還有成長空間，現在的訂位率都在九成以上；南韓、越南布局都陸續傳佳音，隨著中國大陸開始解封，澳門航線預計2、3月要開航，台灣虎航是唯一國內北、中、南機場都飛澳門的業者，要重返國內航空業澳門王地位。 陳漢銘評估，航班只要載客率超過五成即可獲利，受惠今年旅客需求回升，評估16個日本航點應該可以陸續增加布局，東北亞之外，東南亞也是營運重心，繼去年12月開航峴港，今年2月將再加開富國島，未來業績持續看好。 台灣虎航 澳門 南機場