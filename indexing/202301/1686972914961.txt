彭博：加密幣貸款平台Genesis本周將聲請破產
2023-01-19
https://udn.com/news/story/6811/6921704
彭博引述知情人士報導，Genesis Global Capital正在安排最快於本周聲請破產。 這個Digital Currency Group（DCG）旗下的加密幣貸款子公司一直分別和債權人團體秘密協商，以應付流動性危機。彭博先前已報導，Genesis已警告，稱無法成功籌資，恐得聲請破產。 彭博引述知情人士也強調，協商仍在進行中，計畫可能有變。 繼避險基金 Three Arrows Capital倒閉後，Barry Silbert的DCG開始面臨資金壓力。在Genesis存放部分資金的加密幣交易所FTX聲請破產後不久，Genesis於11月暫停了贖回。 由Cameron和Tyler Winklevoss 兄弟經營的加密幣交易所Gemini Trust也遭殃。允許Gemini用戶通過在Genesis平台借出所持代幣賺取收益的Gemini Earn也停止贖回。 根據彭博獲取的消息，DCG在1月17日致股東的信中表示將暫停按季派息以節省現金。 破產 FTX