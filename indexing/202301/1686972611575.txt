倚天酷碁 拚登創新板
2023-01-02
https://udn.com/news/story/7254/6882238
趕在2022年最後一個上班日，宏碁（2353）集團子公司倚天酷碁12月30日向臺灣證券交易所送件申請臺灣創新板掛牌。統計2022年，共有八家提出創新板申請的新創企業。 隨創新板推廣效益逐漸顯現，以Micro-LED創新顯示器技術應用為主的錼創科技-KY創，2022年8月搶得頭香掛牌上市，「能源界Uber」泓德能源、「綠能界波克夏」雲豹能源等再生能源新秀亦已獲證交所董事會審議通過，後續又有台灣虎航、正瀚生技、Gogolook（走著瞧）跟進送件申請掛牌創新板。 截至2022年11月底止，創新板「合格投資人」戶數累計近11萬人，較去年初的6.6萬人成長逾六成，成果卓越。 倚天酷碁以科技與創新開發產品，致力推動數位轉型與促成智慧生活，跨界為不同產業提供產品、服務和解決方案。 創新板 台灣虎航 顯示器