此生必買國民保單額度放寬 建構媽媽基本保障
2023-05-02
https://udn.com/news/story/7239/7136626
政府推動5月1日起小額終老保險放寬保額至90萬元，第一金人壽（2892）表示，媽媽是家中的心靈支柱，壽險是爸媽族的必備款保單，政府推動小額終老保險放寬保額至90萬元，此商品含括高CP值、保費便宜、門檻低、免體檢、終身保障等五大好處，是此生必買國民保單，讓民眾用小額保費，即可為最愛的家人打造終身壽險保障。 全年無休、總是把小孩放第一的媽媽，在母親節除了該好好坐下來享用美食，更應該多為自己想一點。第一金人壽表示，時間有限的媽媽，可以善用第一金人壽24小時網路投保平台，只要滑滑手機就能快速購入保障。 若想讓保障再升級，建議媽媽可用一加一的方式規劃，假設35歲的媽媽投保繳費20年期、保額50萬元的第一金人壽「珍愛e起」小額終老保險 ，再搭配保障期間20年、保額100萬元的第一金人壽「e定好」定期壽險，兩張保單每天只要35元，就能輕鬆擁有150萬元保障額度，建構保障基礎。 第一金 保單 保險