品觀點｜高捷與勞動部開室內及工業配線班  培養機電人才 - 時事	
2024-01-16	
https://www.chinatimes.com/realtimenews/20240116003228-263301	
為提升民眾的就業技能，高捷公司攜手勞動部勞動力發展署高屏澎東分署，共同推動產訓合作訓練模式，開辦「室內及工業配線」班，報名期間自即日起至113年2月16日止，培養所需的機電人才。

高捷表示，這項室內及工業配線班，主要結合企業及就業服務訓練單位資源，規劃培養民眾符合產業需求的職業訓練課程，協助民眾結訓後可立即就業，同時進用高捷公司所需業務人力，透過產訓合作模式，達到民眾、企業與就業服務單位三贏的局面。

高捷為了招募管道更多元化，順利招募相關業務人才，除於人力銀行公開招募及大專院校合作進用實習生與應屆畢業生外，更積極參加就業服務單位辦理的就業媒合活動，長久以來與「勞動部勞動力發展署高屏澎東分署」尋求合作機會，希望共同透過產訓合作模式，訓練並進用軌道產業所需的相關機電人才。

高捷說，產訓合作訓練，是勞動部勞動力發展署高屏澎東分署專門為企業人力需求，客製化的職業訓練計畫，並結合二家以上具共同人力需求的企業，研擬培訓及招募等事宜，由高屏澎東分署及本分司分別提供「專業技能訓練」與「實務訓練」，結訓後由企業直接僱用，除可以讓學員順利接軌職場外，同時培育企業所需人才。

高捷指出，捷運軌道運輸是城市運輸動脈之一，打造優質的軌道輸運品質，有賴質優且充足營運及維護人力。為提升招募誘因，高捷公司積極規劃提高新進人員待遇標準，維修技術員試用期間每月薪資新台幣31,100元起，試用期間三個月，試用期滿每月薪資34,000元起，其他獎金、津貼另計，歡迎高中職畢業有興趣民眾至勞動部高屏澎東分署官網報名參加甄試。
