防颱風淹水 汐止區長視察抽水站備砂包供民索取 | 聯合新聞網	
2023-07-26	
https://udn.com/news/story/7323/7326857	
杜蘇芮颱風逼近，因應可能降下的豪大雨，汐止區公所準備了上千個砂包提供民眾索取，今（26）日汐止區長林慶豐也前往中興路的北峰抽水站視察機組運作狀況，並且呼籲嚴防可能的強風大雨，請民眾要做好防颱準備。汐止區長林慶豐表示，今天特地到中興路的抽水站視察，抽水站共有9處的抽水機組，在颱風來臨之前，針對機組及油料的部分做最後的檢視跟確認，這次的杜蘇芮颱風已經發布陸上及海上的颱風警報，也要呼籲所有的市民朋友請不要前往海域或山域從事相關的活動，確保安全。汐止社后地區一帶只要豪大雨就容易積淹水，尤其中興路涵洞，所以，颱風大雨抽水機組能不夠正常抽水運作很重要，當天區長也在工務課長的陪同下了解抽水情形，而針對杜蘇芮颱風來襲，因應可能降下的豪大雨，山區里長特別重視，也先巡視里內狀況，白雲里長陳俊地說，颱風警報之前都會巡視山區側溝，如果有需要清的都會事先清除，大範圍的就會請公所或清潔隊協助。林慶豐指出，杜蘇芮颱風來襲，新北市政府已經召開三次的整備會議，針對相關側溝的部分，已經進行相關巡檢以及清淤，後續轄管的抽水站也請廠商進行相關的整備工作，公所也備妥了砂包，如果有需要的民眾可以聯繫汐止區公所，呼籲大家一起做好相關的防颱工作。