港官稱無意恢復駐台機構 但緊急事故會與台聯繫
2023-04-12
https://udn.com/news/story/7331/7093752
港人赴台觀光者日增，香港立法會會議上，有議員關注到台港關係未得改善下港人遇事如何求助。對此，港官說港府無打算恢復駐台機構運作，但遇緊急事故香港當局會與台灣當局聯繫提供協助。 台港兩地駐對方的官方機構，一直處於冷凍狀態。在香港立法會財務委員會上，議員關注到有不少港人到台灣旅遊、讀書等，問這些人遇到問題，香港政府如何提供運作。 港府政制及內地事務局局長曾國衞稱，是由於台灣方面「過去一段時間連串行徑，破壞香港與台灣的關係」，港府是在考慮駐台人員的安全等問題後，在前年暫停運作在台設立的香港經濟貿易文化辦事處，在情況未有改善下，港府不打算重新恢復運作，目前雙方並無官方交流。 但曾國衞稱，香港當局歡迎台灣民眾來港旅遊，亦有在當地宣傳「你好，香港」活動。如港人在台遇事，可致電香港入境處求助熱線，如涉及緊急事故，港府會與台灣當局聯繫提供協助。至於經貿推廣方面，香港貿易發展局在台灣設有辦事處，會為在台港商提供支援及資訊。 2月20日起，台灣恢復開放港澳人士自由行來台旅遊後，來台的香港遊客已明顯增多。圖為今年台北燈會。（香港中通社） 香港 觀光