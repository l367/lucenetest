光陽傳奪基隆電動機車標案
2023-04-19
https://udn.com/news/story/7241/7107041
光陽公司昨（18）日指出，光陽電動機車品牌Ionex今天將與基隆市政府宣布重要消息，共同舉辦青年電動機車計畫記者會，外界認為，這是基隆市長謝國樑為兌現去年競選承諾的重要政見之一。 台灣電動機車約占整體市場的一成，而且各方人馬相繼投入，除Gogoro與光陽Ionex之外，宏佳騰、中華車等也都積極推出電動機車各式車款搶攻市場。 市場預期，基隆市的青年電動機車計畫將釋出相當可觀的電動機車需求，對於電動機車業者的攻城掠地有重要意義，也吸引不少業者加入戰局。 由於此次活動由基隆市政府主動通知光陽公司，一般認為，這塊大餅已由光陽公司拿下。 光陽指出，基隆市政府臨時通知，市長謝國樑將出席這場青年電動機車計畫的記者會，內部緊急動員，因此光陽董事長柯勝峯與謝國樑將共同出席，這項計畫今天有具體結論，包括確實數量與價格。 謝國樑提出的青年免費送電動機車政見，吸引多家電動機車品牌競相投標，大廠KYMCO光陽更發出預告，但相關細節尚未透露。 謝國樑是在2月時宣布，將以追加預算的方式，採購1萬輛以上的電動機車，並由地方補助3萬元預算及中央經濟部能源局補助7,000元，環保署補助汰舊換新2,000元或4,000元，總共4萬元額度的電動機車補助。 電動機車 光陽