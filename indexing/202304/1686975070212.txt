機構感控／專家：升級感控攸關人命 應先給錢
2023-04-16
https://udn.com/news/story/7266/7100695
機構住民染疫比率高，疫情期間，不少家屬直接把長輩接回家照顧，就是擔心繼續待在機構，增加染疫致死機率，「送機構容易生病」也成了不少人的刻板印象。專家批評，政府每每推出新制，都要先看到成果才給予補助，但機構的感控攸關人命，強化感控不該是錦上添花，應是雪中送炭，先給錢才有機會全面提升。 全台數千家住宿式機構，多設置在公寓大廈中，目前已知新冠病毒會透過「氣溶膠傳播」，過去曾多次發生過防疫旅館的空調系統，因連通各個房間，新冠病毒以氣溶膠傳播的特性造成多人染疫，機構光是要克服公寓大廈建築結構，讓通風系統達標，花費至少動輒百萬。這也成了衛福部獎勵機構強化感控計畫六大指標，花費最高的項目。 台大職業醫學與工業衛生研究所副教授陳佳堃表示，住宿機構多半設置在大樓中，且建築間距離近，即使開窗也僅相鄰六十公分，空氣流通難度高，再加上國內的空調設備多採「內循環」，能節能卻沒有換氣功能。此外，近期政府調漲電費，為了省錢，開冷氣一定緊閉門窗，省能源卻反而忽略公衛問題。 台大醫院小兒感染科主治醫師黃立民說，長照機構過去在感控上品質良莠不齊，蔡總統注重長照政策，應可活用經費，給予長照機構相關協助。 長照 台大醫院 住宿