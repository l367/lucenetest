陸40城房貸利率降至3字頭
2023-04-24
https://udn.com/news/story/7333/7120484
大陸多地房貸利率持續下探，大陸房市調研機構中指研究院統計數據顯示，目前已經有40多個城市房貸利率進入「3字頭」時代。分析認為，短期來看，市場銷售回暖持續性不足，多個城市二手房價格仍處下跌，或進一步影響新房價格預期。 中指研究院最新監測數據顯示，今年以來，已有太原、金華、鄭州、珠海、石家莊、瀋陽、衢州等40多個城市調整首套房貸款利率下限至4%以下。例如，今年瀋陽首套房貸款利率更下調兩次，先從4.1%降至3.9%，近期調整後進一步降至3.8%。 貝殼研究院發布數據顯示，重點監測的百城首套房主流房貸利率在今年4月平均為4.01%，較上月微降1個基點；二套主流房貸利率平均4.91%，與上月持平。 北京、上海、廣州等一線城市，多家銀行二手房首套房貸款利率仍維持在4%以上。 套房 二手房 北京