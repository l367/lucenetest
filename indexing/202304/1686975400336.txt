台積電再發行綠債207億元 綠建築及環保支出用
2023-04-21
https://udn.com/news/story/7240/7114608
晶圓代工廠台積電今天決議發行新台幣207億元無擔保普通公司債，為綠色債券，將用於綠建築及綠色環保相關的資本支出。 台積電今天公告，預計發行的207億元綠債，其中，5年期的甲類發行金額為131億元，7年期的乙類發行金額23億元，10年期的丙類發行金額53億元。 甲類固定年利率1.6%、乙類固定年利率1.65%、丙類固定年利率1.82%。台積電指出，將委任富邦綜合證券為主辦承銷商，募得資金將用於綠建築及綠色環保相關的資本支出。 因應可能的污染防治相關支出，以及產能擴充的資金需求，台積電董事會於2月14日核准於不高於新台幣600億元的額度內在國內市場募集無擔保普通公司債，3月已發行193億元綠債，今天決定發行的綠債是台積電今年度第2期公司債。 台積電 新台幣 公司債