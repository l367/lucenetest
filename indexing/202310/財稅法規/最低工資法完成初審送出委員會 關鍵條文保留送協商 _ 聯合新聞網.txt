最低工資法完成初審送出委員會 關鍵條文保留送協商 | 聯合新聞網	
2023-10-18	
https://udn.com/news/story/7238/7514271	
立法院社會福利及衛生環境委員會今（18）日審查「最低工資法」，雖順利送出委員會但關鍵條文，包括：參採指標、委員組成、公布程序及罰責等都被保留待後續朝野協商，19條僅8條不用再協商。至於《最低工資法》要不要訂公式？勞動部長許銘春明確表態，參考主要國家的最低工資都是由審議會討論決定，很少是採用公式，因為若遇特殊經濟情勢，將無法審酌，也欠缺彈性。許銘春強調，法令也應要保留彈性，以疫情為例，就發生經濟的突發狀況，因此勞動部主張還是要保留彈性，最低工資不宜用公式來決定。「最低工資法」是蔡總統2016年的勞工政見之一，但勞動部版的最低工資法草案2019年就送入行政院，擱置逾四年，行政院在今年9月底將政院版本送入立法院，立法院社福及衛環委員會今審查草案。但國民黨籍立委林為洲、溫玉霞皆質疑，勞動部版本、行政院版本幾乎沒有不同，政院一再拖延，趕在選前才處理，可見「選舉萬歲！」此外，針對行政院版最低工資法草案第九條將消費者物價指數年增率列為調整幅度的「應」參採指標，並羅列勞動力生產力指數年增率、勞工平均薪資年增率等十項「得」參採指標的關鍵條文，則看法分歧。立委林為洲認為，除消費者物價指數年增率，家庭收支狀況、基本生活費也應列入應參採指標，多個指標做綜合考量。同時，他所提版本還明文「不得低於前一年最低工資」，防止未調漲的情況發生。立委王婉諭也說，應要加設兩道樓地板，作為基本工資調升的最低底線；時代力量黨團提案版本明訂「年度調整幅度不得低於消費者物價指數年增率之漲幅」，且「不得低於前一年最低工資」，如此一來，才能像法國制度，讓基本工資調幅符合物價上漲幅度，保障勞工實質購買力。民進黨籍立委吳玉琴則說，雖知道明訂公式有難度，但為避免每年審議會協商淪為勞資互相喊價，她仍希望能有明確的調整公式，讓勞資雙方能有依循。民眾黨籍立委賴香伶也認為，算式入法，更能取信於勞資雙方。勞動部勞動條件及就業平等司司長黃維琛則表示，若訂公式或設立樓地板，將喪失審議彈性，遇到如金融海嘯、新冠疫情等重大經濟環境變動，恐難以彈性因應。此外，黃維琛也提到，消費者物價指數年增率是調升的最重要考量，但即使當年消費者物價指數上升，也可能會遇到大環境問題，因此CPI要不要十足反映？反映到什麼程度？也還需參酌其他相關數據。不過，黃維琛強調，自民國75年基本工資實施迄今，從未往下調，頂多就是沒有調升。▪ 他讚台中房子新、市容乾淨「台北反而像鄉下」 網揭台中缺點
 ▪ 北市蛋黃區50年公寓「天價裝潢費」曝光 網：一堆吃米不知米價
 ▪ 1坪賣近百萬元創新高紀錄 「木都」嘉義市老屋地點曝
 ▪ 彰化人台中買「帝寶」踩雷 持有7年慘賠2117萬
 ▪ 通勤增20分鐘等同減薪19% 他曝企業選址這點最重要