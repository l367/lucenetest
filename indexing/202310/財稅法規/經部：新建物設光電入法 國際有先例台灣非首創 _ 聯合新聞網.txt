經部：新建物設光電入法 國際有先例台灣非首創 | 聯合新聞網	
2023-10-14	
https://udn.com/news/story/7238/7505236	
網紅「館長」陳之漢指稱，再生能源發展條例通過新建物設置光電為貪汙。經濟部今天發布新聞稿嚴正駁斥貪汙說法指出，為因應全球氣候變遷，德國柏林、日本東京已將建物設置光電入法，台灣並非首創，更非圖利特定廠商及貪汙。立法院會今年5月三讀通過修正再生能源發展條例部分條文，增訂符合一定條件的新建、增建或改建建築物，應在屋頂設置一定裝置容量太陽光電發電設備。經濟部今天說明，在新建建物規劃設計時將光電納入建築設計中，有助提升結構安全及美觀，且一次施工可降低設置成本，並可提供建物頂層隔熱、防水等效益；而透過設置光電，可優先提供公共設施，例如照明及水塔馬達等基本民生系統用電，餘電賣給台電，收益歸社區。經濟部指出，初期並非所有新建建築物屋頂都要設置，目前草案規劃建築面積1000平方公尺（約300坪）以上才需強制設置光電，類似用電大戶應使用一定比例綠電的精神進行規範，並且針對建物受光條件不足及其他特殊情形經過審核後可免除設置。經濟部強調，因應劇烈極端氣候頻率升高，國際淨零碳排潮流已是趨勢，台灣已規劃「2050淨零排放政策路徑藍圖」，一同為地球盡一份心力，減緩氣候變遷速度。▪ 他讚台中房子新、市容乾淨「台北反而像鄉下」 網揭台中缺點
 ▪ 北市蛋黃區50年公寓「天價裝潢費」曝光 網：一堆吃米不知米價
 ▪ 1坪賣近百萬元創新高紀錄 「木都」嘉義市老屋地點曝
 ▪ 彰化人台中買「帝寶」踩雷 持有7年慘賠2117萬
 ▪ 通勤增20分鐘等同減薪19% 他曝企業選址這點最重要