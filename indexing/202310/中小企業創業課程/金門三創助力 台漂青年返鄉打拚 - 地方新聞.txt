金門三創助力 台漂青年返鄉打拚 - 地方新聞	
2023-10-31	
https://www.chinatimes.com/newspapers/20231031000539-260107	
金門以往因產業結構單薄、謀職不易，導致年輕人口外流，但近年來愈來愈多青年願意「洄游」返鄉，縣府透過資源整合、創業補助與福利津貼等政策，讓青年們願意留下來，此外，有不少「台漂」青年選擇返鄉創業，讓金門產業注入更多青春活力。
金門縣政府表示，為讓青年在地區有更好發展，努力打造「美好金門、友善青年」環境，不僅提供青年購屋建屋、青年創業貸款等多元友善政策，為激發年輕人的創新和創業活力，更成立了「金門三創（創新、創意、創業）服務平台」，提供一站式服務，包括在地諮詢輔導、創投媒合、創業家交流會、創業育成課程、趨勢講座、數位學習頻道、產業創新論壇以及青年行銷活動等。
此外，在創業過程中最重要的「資金」，縣府也協助解決申辦創業貸款問題，於2018年邀請地區土銀、台企銀、台銀與金門信用合作社4家金融主、分支機構擔任承貸銀行，共同加入經濟部中小企業信用保證基金「地方政府相對保證專案」，提供創業貸款信用保證融資服務，授權由承貸銀行評估審查，簡化申請流程，讓資金挹注更即時，迄今已成功協助逾170案青年創業承貸。
縣府也表示，青年是城市競爭力的重要支柱，也是地區發展的重要關鍵，目前針對青年創業困難，先行盤點金門各產業在創業上的困難點以及思考因應作法，未來將以「扶青年、育人才、拓市場」為導向，提供一系列專業課程訓練、諮詢輔導、業師陪伴等服務提供專業知能養成。
另透過青年三創平台與產業輔導計畫的支持，讓金門青年可以克服創業過程中的各種困難和挑戰，提高創業成功率，同時也培養青年具備開拓市場的能力，進而驅動未來產業經濟朝永續發展邁進。