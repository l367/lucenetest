移民署歡慶國際女孩日 鼓勵新住民勇敢築夢「做自己」 - 社會	
2023-10-11	
https://www.chinatimes.com/realtimenews/20231011004215-260402	
聯合國為提升全世界女孩應有的人權與照顧，將每年10月11日訂為「國際女孩日」，移民署北區事務大隊臺北市服務站特別於當日舉辦「新住民家庭教育暨法令宣導」課程，邀請來自新疆烏魯木齊的新住民段榮擔任講師，分享她在臺灣勇敢築夢的過程，並鼓勵新住民展現自信「做自己」。

段榮在8年前透過朋友介紹結識臺灣郎，兩人更走上婚姻之路嫁來臺灣。為了適應臺灣的生活，她報名了移民署、勞動局許多培訓課程，並獲得美髮、烘焙等證照。段榮說，這麼多年來，從她自己和身邊的很多朋友，深深感受到臺灣政府和社會組織對新住民的關懷與幫助，內心真的覺得很溫暖，都可以尋求到幫忙與依靠。

新疆四季分明、氣候乾爽宜人，新疆人們能歌善舞、熱情開朗，而臺灣天氣比較潮濕，數度讓段榮興起返鄉的念頭，甚至不敢踏出家門，但為了家庭、為了自己，她開始展開學習，走出自己的舒適圈，也很感恩臺灣人的友善，讓她漸漸融入和愛上了這片土地。

「其實只要為人善良、肯勇於成長，便能令人刮目相待」!段榮說，她也常以此勉勵女兒。女兒從小在中國大陸生長，初來臺灣也很不習慣。女兒在國中、高中時，毛遂自薦地當上了學習委員和班長，甚至競選上學校的司儀，現在已經大學三年級的女兒，非常懂得「做自己」，在成長過程中她實踐了「逐夢踏實」的精神。

臺北市服務站主任蘇慧雯表示，移民署做為新住民的娘家，鼓勵新住民及其子女們無所畏懼、展現女力，第10屆新住民及其子女築夢計畫，已經開始受理報名，歡迎符合資格的新住民朋友踴躍報名，一起築夢，報名期限至今（112）年11月20日止，報名簡章已公布在移民署全球資訊網，或可電洽02-23889393分機2371胡小姐詢問。
