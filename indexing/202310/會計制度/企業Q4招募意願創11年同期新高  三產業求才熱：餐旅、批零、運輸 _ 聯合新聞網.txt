企業Q4招募意願創11年同期新高  三產業求才熱：餐旅、批零、運輸 | 聯合新聞網	
2023-10-03	
https://udn.com/news/story/7238/7480270	
邊境解封即將滿一年，yes123求職網「兔年第四季景氣暨勞動市場趨勢調查」結果顯示，九成企業今年第四季有徵才計畫，創下11年以來同期新高，徵才平均規模13人。從產業別觀察， 求才最熱三大產業依序是餐旅、批發零售、運輸業。調查發現，90.2%企業今年第四季有徵才計畫，雖然略少於Q3招募比率92.4%，但明顯多過去年同期的79.1%，更創下11年以來同期新高。「產業別」交叉分析，Q4企業「徵才意願較高」的行業，依序為：「餐飲住宿與休閒旅遊」(95.2%)、「批發零售與貿易」(94.4%)、「運輸與物流倉儲」(93.1%)，以及「金融保險與會計統計」(92%)、「科技資訊」(91.3%)。第四季應徵新員工的原因，依序為：「一直處於缺工狀態」(38.9%)、「無擴編，單純填補人員離職空缺」(25.2%)，以及「訂單或業務增加而擴編」(19.8%)、「公司擴點、展店而擴編」(16.1%)。合計「要擴編」的企業比率35.9%，雖然略少於前一季(Q3)的36.4%，不過明顯多於去年Q4的28.3%，還創下九年同期新高，僅低於2014年第四季的39%。資方Q4「缺工比率」為38.9%，不僅略高於前一季的38.5%，也多於去年同期的34.5%，創下11年以來同期新高。平均而言，每一家企業對人才的需求為12.8人，規模小於前一季的15.3人，但仍多於去年同期的12人。在可複選情況下，今年第四季計畫徵才的企業，它們釋出的主要職缺類型，又分成62.8%屬於「基層員工」；屬於「第一線門市或銷售、業務人員」的，也占42.2%；屬於「產線作業員」部分的有32%；屬於「儲備幹部」的占25.8%；屬於「工讀生或臨時工、計時人員」也有24.4%。針對這些主要職缺，企業平均提供的月薪條件，落在42,299元，明顯高於前季(Q3)調查時的38,310元，也多於去年Q4的41,443元，續創十一年同期新高。yes123求職網發言人楊宗斌預估，調查顯示企業仍看好邊境解封後，帶動報復性旅遊潮、購物熱，因此各大產業求才在第四季將是「內熱外冷」。從國慶連假開始，到百貨週年慶與聖誕、跨年檔期，內需型店家面臨缺工潮，所以不斷有徵人需求，只是其中有不少的工作，可能屬於臨時性職缺。楊宗斌認為，部份科技業或製造業公司，受制於全球終端產品歲末買氣不佳，資方庫存壓力，各行各業的職缺有增有減，第四季整體起薪的波動尚稱平穩。▪ 他讚台中房子新、市容乾淨「台北反而像鄉下」 網揭台中缺點
 ▪ 北市蛋黃區50年公寓「天價裝潢費」曝光 網：一堆吃米不知米價
 ▪ 1坪賣近百萬元創新高紀錄 「木都」嘉義市老屋地點曝
 ▪ 彰化人台中買「帝寶」踩雷 持有7年慘賠2117萬
 ▪ 通勤增20分鐘等同減薪19% 他曝企業選址這點最重要