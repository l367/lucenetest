崇越集團董事長郭智輝 獲聘臺北大學客座講座教授 | 聯合新聞網	
2023-10-20	
https://udn.com/news/story/7238/7517813	
崇越今（20）日指出，集團董事長郭智輝博士獲聘臺北大學客座講座教授10月19日於崇越科技總公司舉行「國立臺北大學郭智輝客座講座教授聘任典禮」，由臺北大學李承嘉校長親自頒發聘任證書。李承嘉校長於聘任典禮中表示，感謝郭智輝博士成功領導崇越科技之餘，更不忘提攜母校學弟妹，擔任職涯導師不遺餘力，將創新卓越的實務經驗帶進校園。崇越提到，郭智輝於聘任典禮上，對臺北大學校務提供發展建言，分析現今台灣經濟局勢與企業發展現況，並分享其擔任「台灣新東向全球產學研聯盟協進會」理事長，助力台商赴美投資，協助台灣中小企業了解在美國發展所需的制度、稅法與ESG規範之經驗歷程。郭智輝博士為臺北大學第4屆傑出校友，於1990年創立崇越科技，帶領企業深耕半導體與光電材料產業，近年積極發展環保綠能以及大健康事業，並積極擘畫海外市場，使崇越科技躋身全方位國際化集團；其經營績效卓著，曾榮獲「第9屆國家卓越成就獎」、「第19屆國家十大傑出經理人總經理獎」以及「2017年安永企業家獎年度大獎」、「2022、2020年《哈佛商業評論》台灣企業領袖100強」等國家級榮耀肯定。📢 蘋果新品／超好看！太空黑色M3版MacBook Pro來了 新24吋iMac速度快2倍
 📢 免費仔快看！YouTube廣告攔截器禁用 他推新外掛「光速看完廣告」成效超讚
 📢 【周報】AirPods單耳沒電原因是它
 📢 5款限免LINE貼圖！喔熊、我是馬克爽用到明年兒童節
 📢 「下載eSIM」新功能終於免費了？中華電信1句話讓網氣瘋：台灣沒救了