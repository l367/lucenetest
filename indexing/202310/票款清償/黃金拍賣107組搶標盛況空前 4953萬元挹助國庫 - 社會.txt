黃金拍賣107組搶標盛況空前 4953萬元挹助國庫 - 社會	
2023-10-31	
https://www.chinatimes.com/realtimenews/20231031002878-260402	
行政執執行署桃園分署8月查封30公斤黃金，今舉行黃金拍賣會，吸引107組民眾進場搶標，有的甚至遠從台中、彰化、高雄等地前來搶購，盛況空前，最終拍定價落在617萬至620萬元，24塊黃金拍出即已清償欠款，拍定總價高達4953萬元，拍賣所得將全數挹助國庫。

財政部關務署台北關3年前查扣黃金條塊30條、每條重達1公斤，行政執行署桃園分署8月接手後，選定萬聖節在桃園分署新址「桃園市桃園區復興路186號」首次辦理拍賣會，拍賣官特地以萬聖節魔女造型登場，現場吸引107組民眾進場搶標，有的甚至遠從台中、彰化、高雄等地前來搶購黃金，盛況空前，由於拍賣室沒有擺放座椅的空間，應買人只能站著喊價。

拍賣總共分14個標別，前8標均以3條為一標的方式拍賣，起標價為520萬元，自第1標開始，應買人即熱情踴躍地喊價，共出價12次才拍定，競爭相當激烈，因喊價情況熱烈，本次拍賣程序進行到第8標，已足清償債權總額，第9標後順序各標依法停拍，執行官指出，若以單次拍賣黃金總市值而言，在法拍史上應該是頭一遭，成果相當豐碩，最終拍定價落在617萬至620萬元間。

經過一番激烈競價後，24塊黃金拍出即已清償欠款，拍定總價高達4953萬元，拍賣所得將全數挹助國庫。